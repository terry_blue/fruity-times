package uk.co.blueshroom.fruitytimes.objects;

/**
 * Created by Terry on 12/07/2015.
 */
public class Message {
    public enum Tier { LOW, MED, HIGH };
    final public Tier PRIORITY;
    final public int R_COUNT;
    final public String MESSAGE;

    public Message(String msg, Tier prity, int repeat) {
        MESSAGE = msg;
        PRIORITY = prity;
        R_COUNT = (repeat == -1) ? -1 : repeat * 2;
    }
}

