package uk.co.blueshroom.fruitytimes.objects;

import uk.co.blueshroom.fruitytimes.objects.Interfaces.StaticReelIconInterface;
import android.animation.Animator.AnimatorListener;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;

public abstract class Icon implements StaticReelIconInterface {

	@SuppressWarnings("unused")
	final private static String TAG = "Icon";
	
	Paint iconPaint;
	int duffColorFilter;
	public Rect mainDestRect, specialDestRect, specialSrcRect;
	protected float ic_index;
	protected boolean isSpecial;
	
	ObjectAnimator flashAnimator;
	
	public Icon() {
		specialDestRect = new Rect();
		mainDestRect = new Rect();
		iconPaint = new Paint();
		isSpecial = false;
		
		int yellow1 = Color.argb(0x00, 0xff, 0xff, 0x42);
		int yellow2 = Color.argb(0xdd, 0xff, 0xff, 0xa8);
		int white = Color.argb(0xe2, 0xff, 0xff, 0xff);
		int zero = Color.argb(0x00, 0xff, 0xff, 0xff);
		
		flashAnimator = ObjectAnimator.ofObject(this, "duffColorFilter", new ArgbEvaluator(), yellow1, yellow2, white, zero);
		flashAnimator.setDuration(200);
	}
	
	public void draw(Canvas canvas) {
		if(getReelBitmap() != null) {
			canvas.drawBitmap(getReelBitmap(), getReelIconSrcRect(), mainDestRect, iconPaint);
			
			if(isSpecial) {
				canvas.drawBitmap(getSpecialIconBitmap(), null, specialDestRect, null);
			}
		}
		
	}
	
	@Override
	public Rect getReelIconSrcRect() {
		if(getReelBitmap() != null) {
			float height = (float)getReelBitmap().getHeight() / (float)getReelIconCount();
			Rect iconSrcRect = new Rect(0, (int)((int)ic_index * height), getReelBitmap().getWidth(), (int)(((int)ic_index + 1f) * height));
			return iconSrcRect;
		}
			
		return new Rect();
	}
	
	public void setDuffColorFilter(int color) {
		iconPaint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
	}
	
	
	public void doFlash() {
		flashAnimator.start();
	}
	
	
	public void setDestRect(Rect rect) {
		setDestRect(rect.left, rect.top, rect.right, rect.bottom);
	}
	public void setDestRect(int left, int top, int right, int bottom) {
		mainDestRect.set(left, top, right, bottom);
		
		specialDestRect.set(mainDestRect);
		specialDestRect.inset((int)(((float)mainDestRect.width() * 0.75f) / 2f), (int)(((float)mainDestRect.height() * 0.75f) / 2f));
		specialDestRect.offset((int)((float)mainDestRect.width() * 0.375f), (int)((float)mainDestRect.height() * 0.375f));
		
		
		
	}
	
	public void addAnimatorUpdateListener(AnimatorUpdateListener updater) {
		flashAnimator.addUpdateListener(updater);
	}
	public void addAnimatorListener(AnimatorListener listener) {
		flashAnimator.addListener(listener);
	}
	
	
	public void setIndex(float newIndex) {
		ic_index = newIndex;
	}
	public float getIndex() {
		return ic_index;
	}
	
}
