package uk.co.blueshroom.fruitytimes.views;

import uk.co.blueshroom.fruitytimes.objects.FeatureIcon;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.blueshroom.fruitytimes.R;

public class FeatureIndicatorGroup extends View {
	
	final static String TAG = "FeatureIndicatorGroup";
	Bitmap imgBmp;
	Rect imgSrcRect, imgDestRect, textRect;
	
	String textTitle = "";
	Paint textPaint;
	
	final static int IC_COUNT = 3;
	FeatureIcon[] icons = new FeatureIcon[IC_COUNT];

	public FeatureIndicatorGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		imgSrcRect = new Rect();
		imgDestRect = new Rect();
		textRect = new Rect();
		
		textPaint = new Paint();
		
		
		//DEBUG
		imgBmp = BitmapFactory.decodeResource(getResources(), R.drawable.fruitreel_special_icon_fruits);
		
		for(int i = 0; i < IC_COUNT; i++) 
			icons[i] = new FeatureIcon();
		
		
		
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {		
		float height = MeasureSpec.getSize(heightMeasureSpec);
		Log.e(TAG, "height " + height);
		int textOffset = (int)(((float)MeasureSpec.getSize(heightMeasureSpec) / 8f));
		Log.e(TAG, "textOffset = " + textOffset);
		int width = MeasureSpec.getSize(widthMeasureSpec);		
		
		int iconHeight = MeasureSpec.getSize(heightMeasureSpec) - textOffset;
		int length = (iconHeight < width / IC_COUNT) ? iconHeight : width / IC_COUNT;		
		
		
		int bottom = textOffset + length;
		
		textRect.set(0, 0, width, textOffset);
		
		icons[0].setDestRect(0
							, textOffset
							, length
							, bottom);
		
		icons[1].setDestRect((width / 2) - (length / 2)
							, textOffset
							, (width / 2) + (length /2)
							, bottom);
		
		icons[2].setDestRect(width - length
							, textOffset
							, width
							, bottom);
		
		
		
		
		if(!textTitle.equals("")) {
			
		}
		
		
		
		
		setTitle(textTitle);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(length + (textOffset / 2), MeasureSpec.AT_MOST));
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawText(textTitle, textRect.centerX() - ((textPaint.measureText(textTitle)) / 2), textRect.centerY() + ((textRect.height() / 3) * 2), textPaint);
		for(FeatureIcon icon : icons) 
			canvas.drawBitmap(imgBmp, null, icon.getMainDestRect(), icon.getIconPaint());
		
		
	}
	
	public void setIconBitmap(Bitmap bmp) {
		imgBmp = bmp;
		invalidate();
	}
	
	/**
	 * Activates the next available icon in the group 
	 * @return true if the set is completed after activating another icon
	 */
	public boolean addToIcons() {
		for(FeatureIcon icon : icons) {
			if(!icon.getIsActive()) {
				icon.setEnabled(true);
				invalidate();
				break;
			}
			
		}
		
		return isCollectionComplete();
	}
	
	Rect boundRect = new Rect();
	public void setTitle(String title) {
		textTitle = title;
		
		float textSize = 8;
		float height = 0;
		
		
		//textRect.inset((int)((float)textRect.width() / 10f), (int)((float)textRect.height() / 10f));
		
		do { 
			textPaint.setTextSize(++textSize);
			textPaint.getTextBounds(textTitle, 0, textTitle.length(), boundRect);	
			
			height = ((textPaint.getFontMetrics().descent - textPaint.getFontMetrics().ascent) + textPaint.getFontMetrics().leading);
			Log.e(TAG, "boundRect.height = " + boundRect.height() + ", height = " + height);
			
		} while(boundRect.height() < (textRect.height()) && boundRect.width() < textRect.width());
		
		//textPaint.setTextSize(textSize-2);
	}
	
	
	public boolean addToIcons(int index) {
		icons[index % icons.length].setEnabled(true);
		
		return isCollectionComplete();
	}
	
	
	public boolean isCollectionComplete() {
		for(FeatureIcon icon : icons) {
			if(!icon.getIsActive()) {
				return false;
			}
		}
		
		return true;
	}
	public void clearAllIcons() {
		for(FeatureIcon icon : icons) {
			icon.setEnabled(false);
		}
		invalidate();
	}
}
