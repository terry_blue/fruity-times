package uk.co.blueshroom.fruitytimes.data;

import java.util.Random;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import uk.co.blueshroom.fruitytimes.data.DataBaseImporter;

public class Spins_DB extends DataBaseImporter {
	
	final static String TAG = "Spins_DB";
	final static int VERSION = 1;
	
	public static enum WinType { BLANK, FEATURE, LOW, MED, RARE };
	
	final static String TABLE_NAME = "Spins";
	final static String COL_ID = "_id";
	final static String COL_REEL = "ReelCode";
	final static String COL_CATEGORY = "Category";
	final static String COL_COUNT = "Count";
	
	
	public Spins_DB(Context context) {
		super(context, "db_reels", VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public float[][] getReelSpin(WinType winType) {
		String where = COL_CATEGORY+"='";
		switch(winType) {
		case BLANK: where += "BLANK'"; break;
		case FEATURE: where += "FEATURE'"; break;
		case LOW: where += "LOW'"; break;
		case MED: where += "MED'"; break;
		case RARE: where += "RARE'"; break;
		}
		
		
		String[] columns = new String[] { COL_ID, COL_REEL, COL_COUNT };
		Cursor c = sqLiteDb.query(TABLE_NAME, columns, where, null, null, null, COL_COUNT);		
		c.moveToPosition(new Random().nextInt(c.getCount()));
		
		String reelCode = c.getString(c.getColumnIndex(COL_REEL));
		int newCount = c.getInt(c.getColumnIndex(COL_COUNT));
		
		String[][] reelSpin = new String[3][3];
		String[] reels = reelCode.split("/");
		
		for(int i = 0; i < reels.length; i++) 
			reelSpin[i] = reels[i].split(",");
		
		float[][] returnSpin = new float[3][3];
		for(int i = 0; i < reelSpin.length; i++) {
			for(int a = 0; a < reelSpin[i].length; a++) {
				returnSpin[i][a] = Float.parseFloat(reelSpin[i][a]);
			}
		}
		
		//Update Count
		ContentValues cv = new ContentValues();
		cv.put(COL_COUNT, newCount);
		sqLiteDb.update(TABLE_NAME, cv, COL_ID+"="+c.getString(c.getColumnIndex(COL_ID)), null);
		
		return returnSpin;
	}

	public static class SpinRow {
		int _id;
		String codeReel;
		String category;
		int count;
		
		public SpinRow(int id, String reel, String cat, int amnt) {
			_id = id;
			codeReel = reel;
			category = cat;
			count = amnt;
		}
	}
}
