package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.views.NumberView;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blueshroom.fruitytimes.R;

public class FragmentBank extends Fragment {

	final static String TAG = "FragmentBank";
	final static String SP_KEY_BANK_SCORE = "uk.co.blueshroom.fruitytimes.fragments.bank.score";

    NumberView scoreView;
		
	SharedPreferences sp;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_bank, container);
		scoreView = (NumberView)view.findViewById(R.id.bank_numberview_score);
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		scoreView.startNumber(sp.getInt(SP_KEY_BANK_SCORE, 0));
		
	}
	
	public void addToBank(int amountToAdd) {
		scoreView.addToNumber(amountToAdd);
		
		SharedPreferences.Editor edit = sp.edit();
		edit.putInt(SP_KEY_BANK_SCORE, scoreView.getNumber());
		edit.apply();
	}
}
