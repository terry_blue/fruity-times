package uk.co.blueshroom.fruitytimes.views;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.IntEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class NumberView extends TextView implements AnimatorListener {

	int number = 0;
	ObjectAnimator animator;
	NumberEmptiedListener numberEmptiedListener = null;
	
	
	public NumberView(Context context, AttributeSet attrs) {
		super(context, attrs);

		setNumber(number);
	}
	
	public void setNumber(int newNumber) {
		setText(Integer.toString(newNumber));
	}
	
	public void addToNumber(int amnt) {
		ObjectAnimator animator = ObjectAnimator.ofObject(this, "number", new IntEvaluator(), number, number + amnt);
		animator.setDuration(700);
		animator.start();
		 
		number += amnt;
	}
	
	public void startNumber(int amnt) {
		number = amnt;
		ObjectAnimator animator = ObjectAnimator.ofObject(this, "number", new IntEvaluator(), 0, amnt);
		animator.setDuration(700);
		animator.start();	
	}
	
	public void emptyNumber() {
		animator = ObjectAnimator.ofObject(this, "number", new IntEvaluator(), number, 0);
		animator.setDuration(1000);
		animator.addListener(this);
		animator.start();
	}

	public int getNumber() {
		return number;
	}
	
	
	public void addEmptyListener(NumberEmptiedListener listener) {
		numberEmptiedListener = listener;
	}
	
	
	
	


	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationEnd(Animator animation) {
		if(numberEmptiedListener != null)
			numberEmptiedListener.onNumberEmptied();
		
	}

	@Override
	public void onAnimationRepeat(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animator animation) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	
	
	
	public interface NumberEmptiedListener {
		void onNumberEmptied();
	}

}

