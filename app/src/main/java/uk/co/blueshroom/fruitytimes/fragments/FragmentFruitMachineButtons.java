package uk.co.blueshroom.fruitytimes.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshroom.fruitytimes.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentFruitMachineButtons.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentFruitMachineButtons extends Fragment implements View.OnClickListener{

    private OnFragmentInteractionListener mListener;

    public FragmentFruitMachineButtons() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fuit_machine_buttons, container, false);

        int[] btnIDs = new int[] { R.id.fmachine_btn_hold1, R.id.fmachine_btn_hold2, R.id.fmachine_btn_hold3, R.id.fmachine_btn_stake, R.id.fmachine_btn_spin };
        for(int i : btnIDs)
            view.findViewById(i).setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(mListener != null)
            mListener.onFragmentViewInteraction(v.getId());
    }

    public void setStake(int stake) {
        ((TextView)getView().findViewById(R.id.fmachine_tv_stake)).setText(Integer.toString(stake));
    }

    public void setHoldReelEnabled(boolean enabled) {
        int[] btns = new int[] { R.id.fmachine_btn_hold1, R.id.fmachine_btn_hold2, R.id.fmachine_btn_hold3 };
        for(int i : btns)
            getView().findViewById(i).setEnabled(enabled);
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentViewInteraction(int viewID);
    }

}
