package uk.co.blueshroom.fruitytimes.fragments;

import java.util.ArrayList;
import java.util.Iterator;

import uk.co.blueshroom.fruitytimes.fragments.FragmentMessages.Message.Tier;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.blueshroom.fruitytimes.R;

public class FragmentMessages extends Fragment implements AnimationListener {
	
	final public static String TAG = "FragmentMessages";
	
	ArrayList<Message> messageList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_messages, container);
		
		messageList = new ArrayList<Message>();
		addNewMessage(new Message("Fruity Times", Tier.LOW, -1));				 
	
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();	
		
		Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_messages_anim1);
		animation.setAnimationListener(this);
		
		
		getView().findViewById(R.id.messages_textview_message).setAnimation(animation);
	}

	
	
	public void addNewMessage(Message msg) {
		messageList.add(msg);
	}
	
	int tierPos = 0;
	final Tier[] PRIORITY_PATTERN = new Tier[] { Tier.HIGH, Tier.MED, Tier.HIGH, Tier.MED, Tier.HIGH, Tier.LOW };
	public Message getNextMessage() {
		if(!messageList.isEmpty()) {			
			int trapCount = 0;
			while(trapCount < PRIORITY_PATTERN.length + 1) {
				Iterator<Message> it = messageList.iterator();
				while(it.hasNext()) {
					Message msg = it.next();
					if(msg.priority == PRIORITY_PATTERN[tierPos]) { 
						it.remove();
						if(msg.repeatCount == -1) addNewMessage(new Message(msg.message, msg.priority, -1));
						else if(msg.repeatCount > 1) addNewMessage(new Message(msg.message, msg.priority, msg.repeatCount-1));
						
						if(tierPos < PRIORITY_PATTERN.length - 1) tierPos++; else tierPos = 0;
						return msg;
					}
				}
				
				if(tierPos < PRIORITY_PATTERN.length - 1) tierPos++; else tierPos = 0;
				trapCount++;
			}
		}
		
		return new Message("HAHA, You Broke Me!", Tier.HIGH, -1);
	}
	

	@Override
	public void onAnimationEnd(Animation anim) {
		if(getView() != null) {
			TextView messageTV = (TextView)getView().findViewById(R.id.messages_textview_message);
			Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_messages_anim1);
			animation.setAnimationListener(this);
			messageTV.startAnimation(animation);
			messageTV.setText("");
		}
	}


	@Override
	public void onAnimationRepeat(Animation arg0) {
		
	}


	@Override
	public void onAnimationStart(Animation arg0) {
		TextView messageTV = (TextView)getView().findViewById(R.id.messages_textview_message);
		messageTV.setText(getNextMessage().message);
		
		//TODO change to set text appearance
		messageTV.setTextColor(Color.CYAN);
		
	}
	
	
	public static class Message {
		public enum Tier { LOW, MED, HIGH };
		Tier priority;
		int repeatCount;
		String message;
		
		public Message(String msg, Tier prity, int repeat) {
			message = msg;
			priority = prity;
			repeatCount = (repeat == -1) ? -1 : repeat * 2;
		}
	}
}
