package uk.co.blueshroom.fruitytimes.views;

import java.util.Random;

import uk.co.blueshroom.fruitytimes.objects.BoardsTheme;
import uk.co.blueshroom.fruitytimes.objects.DiceIcon;
import uk.co.blueshroom.fruitytimes.objects.Interfaces.DiceReelListener;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;

public class DiceReel extends Reel {

	final static String TAG = "DiceReel";
	final public static int[] DICE_VALUES = new int[] { 7, 3, 10, 1, 5, 12, 8, 2, 6, 11, 9, 4 };
	
	DiceReelListener diceReelListener;
	
	
	public DiceReel(Context context, AttributeSet attrs) {
		super(context, attrs);
		for(int i = 0; i < MIDDLE_IC_COUNT; i++) { 
			middleReelIcons[i] = new DiceIcon(new Random().nextInt(8)); 
	
			if(i < displayIcons.length) {
				displayIcons[i] = new DiceIcon(new Random().nextInt(8));
				displayIcons[i].addAnimatorUpdateListener(this);
			}
			
			if(i < topReelIcons.length) 
				topReelIcons[i] = new DiceIcon(new Random().nextInt(8));
			
			if(i < bottomReelIcons.length) 
				bottomReelIcons[i] = new DiceIcon(new Random().nextInt(8));
		}
		
		
		redrawSpinBitmap();
		initializeAnimators();

	}

	
	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec) / 3; //BAD CODE!! find a way to remove the 3
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int length = (width < height / getDisplayedIconCount()) ?  width : height / getDisplayedIconCount();
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));

		if(spinBitmap != null) {
			final int IC_COUNT = topReelIcons.length + middleReelIcons.length + bottomReelIcons.length;
			drawSrcRect.set(0, spinBitmap.getHeight() - ((spinBitmap.getHeight() / IC_COUNT) * displayIcons.length), spinBitmap.getWidth(), spinBitmap.getHeight());


			drawDestRect.set((width - length) / 2
					, (height - (length * getDisplayedIconCount())) / 2
					, width - ((width - length) / 2)
					, height - ((height - (length * getDisplayedIconCount())) / 2));


			setIconDestRects();
			//initializeAnimators();
			//redrawSpinBitmap();
		}
	}
	
	
	public void setDiceReelListener(DiceReelListener listener) {
		diceReelListener = listener;
	}

	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onAnimationEnd(Animator animation) {
		isSpinning = false;
		diceReelListener.onDiceSpinFinished();
		
	}


	@Override
	public void onAnimationRepeat(Animator animation) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onAnimationStart(Animator animation) {
		isSpinning = true;
		
	}


	@Override
	public void onAnimationUpdate(ValueAnimator arg0) {
		invalidate();
		
	}


	@Override
	public Bitmap getReelCoverBmp() {
		return BoardsTheme.getDiceReelCoverBitmap();
	}


	@Override
	public Rect getReelCoverSrcRect() {		
		return BoardsTheme.getDiceReelCoverSrcRect();
	}

	@Override
	public int getDisplayedIconCount() {	
		return 1;
	}
}
