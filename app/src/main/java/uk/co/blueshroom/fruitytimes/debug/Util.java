package uk.co.blueshroom.fruitytimes.debug;

import android.util.Log;

public class Util {

	final public static String TAG_PREFIX = "DEBUG_UTIL: ";
	
	public static void LogReel(String TAG, float[][] reel) {
		Log.e(TAG_PREFIX + TAG, "reel = " + reel[0][0] + "," + reel[0][1] + "," + reel[0][2]
								+ "//" + reel[1][0] + "," + reel[1][1] + "," + reel[1][2]
								+ "//" + reel[2][0] + "," + reel[2][1] + "," + reel[2][2]);
	}
}
