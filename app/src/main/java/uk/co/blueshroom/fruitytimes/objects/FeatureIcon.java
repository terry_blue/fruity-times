package uk.co.blueshroom.fruitytimes.objects;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;

public class FeatureIcon extends Icon {	
	
	
	final static int COLOR_ENABLED = 0x00000000;
	final static int COLOR_DISABLED = 0x99cccccc;
	private boolean isActive;
		
	public FeatureIcon() {		
		setEnabled(false);
	}
	
	public void setEnabled(boolean isEnabled) {
		isActive = isEnabled;
		setDuffColorFilter((isEnabled) ? COLOR_ENABLED : COLOR_DISABLED);
	}
	public boolean getIsActive() {
		return isActive;
	}
	
	
	public Paint getIconPaint() {
		return iconPaint;
	}
	public Rect getMainDestRect() {
		return mainDestRect;
	}
	


	@Override
	public Bitmap getReelBitmap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bitmap getSpecialIconBitmap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getReelIconCount() {
		// TODO Auto-generated method stub
		return 0;
	}

}
