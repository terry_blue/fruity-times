package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.views.FruitIconIndicator;

import android.app.Fragment;
import android.graphics.Bitmap;

public abstract class IndicatorFragment extends Fragment {
	
	final static String TAG = "IndicatorFragment";
	FruitIconIndicator[] INDICATORS = null;
	
	
	@Override
	public void onStart() {
		super.onStart();
		
		allIndicatorsOff();
	}
	
	public void setReelBitmap(Bitmap reel_bitmap, int reel_icon_count) {
		if(reel_icon_count == 1) {
			for(FruitIconIndicator indi : INDICATORS)
				indi.setSrcRect(0,0,reel_bitmap.getWidth(), reel_bitmap.getHeight());
		} else {		
			for(int i = 0; i < INDICATORS.length; i++) {
				INDICATORS[i].setSrcRect(0, (int)((float)i * ((float)reel_bitmap.getHeight() / (float)reel_icon_count))
						 , reel_bitmap.getWidth(), (int)((float)(i + 1) * ((float)reel_bitmap.getHeight() / (float)reel_icon_count)));
			}
		}
	}
	
	public void setIndicator(int indi, boolean on) {
		INDICATORS[indi].setEnabled(on);
	}
	public void allIndicatorsOff() {
		for(FruitIconIndicator indi : INDICATORS) 
			indi.setEnabled(false);
	}
	public boolean isCollectionComplete() {
		for(FruitIconIndicator indi : INDICATORS) {
			if(!indi.isEnabled)
				return false;
		}
		return true;
	}
}
