package uk.co.blueshroom.fruitytimes.fragments;

import java.util.Random;

import uk.co.blueshroom.fruitytimes.objects.Interfaces.DiceReelListener;
import uk.co.blueshroom.fruitytimes.views.BoardIcon;
import uk.co.blueshroom.fruitytimes.views.DiceReel;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blueshroom.fruitytimes.R;

public class FragmentBoardIcons extends Fragment implements AnimatorListener, DiceReelListener {

	final static String TAG = "FragmentBoardIcons";
	public enum IconType { MULTI_ADD, ADD_CASH, ADD_HITS, LOSE, CHANCE, START, XTRA, IC_HITS, IC_BONUS } //Order determined by the order of the icon set images resources
	
	final int MAX_ICONS = 12;
	
	
	BoardIcon[] boardIcons = new BoardIcon[MAX_ICONS];
	LandingListener landingListener;
    DiceReel diceReel;
	
	int curPosition;
	int amntLeftToMove;
	int displayedDiceValue = 0;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_board_icons, container);
		
		curPosition = 0;
		amntLeftToMove = 0;
		diceReel = (DiceReel)view.findViewById(R.id.board_reel_dice);
		diceReel.setDiceReelListener(this);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		setIcons();
	}
	
	
	private void setIcons() {
        if(getView() != null) {
            int[] resIds = new int[]{R.id.board_boardicon_start, R.id.board_boardicon_add_cash_1, R.id.board_boardicon_hits_ic_1, R.id.board_boardicon_add_hits_1, R.id.board_boardicon_xtra_life, R.id.board_boardicon_chance
                    , R.id.board_boardicon_multi_add, R.id.board_boardicon_add_cash_2, R.id.board_boardicon_hits_ic_2, R.id.board_boardicon_add_hits_2, R.id.board_boardicon_bonus_ic, R.id.board_boardicon_lose};
            IconType[] iconTypes = new IconType[]{IconType.START, IconType.ADD_CASH, IconType.IC_HITS, IconType.ADD_HITS, IconType.XTRA, IconType.CHANCE
                    , IconType.MULTI_ADD, IconType.ADD_CASH, IconType.IC_HITS, IconType.ADD_HITS, IconType.IC_BONUS, IconType.LOSE};

            for (int i = 0; i < MAX_ICONS; i++) {
                boardIcons[i] = (BoardIcon) getView().findViewById(resIds[i]);
                boardIcons[i].setIcon(iconTypes[i]);
                boardIcons[i].setAnimatorListener(this);
            }

            move(0);
        }
	}

	
	public void spinDice() {
		if(!getIsMoving() && !getIsDiceSpinning()) {
			int newDiceIndex;
			do newDiceIndex = new Random().nextInt(12);
			while(DiceReel.DICE_VALUES[newDiceIndex] == displayedDiceValue);		
			displayedDiceValue = DiceReel.DICE_VALUES[newDiceIndex];
			
			diceReel.spin(new float[] { newDiceIndex });
			
			amntLeftToMove = DiceReel.DICE_VALUES[newDiceIndex];
		}
	}
	
	public void move(int amnt) {
		if(amnt != 0) {
			amntLeftToMove = (amnt > 0) ? amnt - 1 : amnt + 1;		
			curPosition = (curPosition < (MAX_ICONS - 1)) ? curPosition + 1 : 0;			
		}
		
		
		boardIcons[curPosition].doPing();
	}
	
	public boolean getIsMoving() {
		return (amntLeftToMove > 0);
	}
	
	public boolean getIsDiceSpinning() {
		return diceReel.getIsSpinning();
	}
	
	
	
	
	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationEnd(Animator animation) {
		if(amntLeftToMove > 0)
			move(amntLeftToMove);
		else {
			boardIcons[curPosition].doIdleFlash();
			landingListener.onIconLand(boardIcons[curPosition].ic_type);			
		}
		
	}

	@Override
	public void onAnimationRepeat(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animator animation) {
		for(BoardIcon icon : boardIcons)
			icon.stopIdleFlash();
		
	}

	@Override
	public void onDiceSpinFinished() {
		move(amntLeftToMove);
		
	}
	
	
	
	public void setLandingListener(LandingListener listener){
		landingListener = listener;
	}
	
	
	public interface LandingListener {
		void onIconLand(IconType icon);
	}
}
