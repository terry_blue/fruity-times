package uk.co.blueshroom.fruitytimes.objects;

import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;
import android.graphics.Rect;

public class ObjUtil {

	public static class RectEvaluator implements TypeEvaluator<Rect> {
		
		@Override
		public Rect evaluate(float fraction, Rect startValue, Rect endValue) {
			int top = startValue.top + (int)((fraction * (float)(endValue.top - startValue.top)));
			int left = startValue.left + (int)((fraction * (float)(endValue.left - startValue.left)));
			return new Rect(left, top, left + startValue.width(), top + startValue.height());
		}
		
	}
	
	
	public static class ShakeInterpolator implements TimeInterpolator {

		@Override
		public float getInterpolation(float input) {			
			return (float)Math.sin(25f * input);
		}
		
	}
}
