package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.views.FruitIconIndicator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blueshroom.fruitytimes.R;

public class FragmentFeatureIconIndicator extends IndicatorFragment {

		
	public FragmentFeatureIconIndicator() {
		INDICATORS = new FruitIconIndicator[3];
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_feature_icon_indicator, container);
		
		int[] icon_ids = new int[] { R.id.featureiconindicator_indicator_1, R.id.featureiconindicator_indicator_2, R.id.featureiconindicator_indicator_3 };
		for(int i = 0; i < INDICATORS.length; i++) {
			INDICATORS[i] = (FruitIconIndicator)view.findViewById(icon_ids[i]);
		}
		
		return view;
	}
	
	public void addToIndicators(float[][] reel_spin) {
		for(int i = 0; i < reel_spin.length; i++) {
			for(int a = 0; a < reel_spin[i].length; a++) {
				if((reel_spin[i][a] * 10f) % 10 != 0)
					setIndicator(i, true);
			}
		}
	}
	
	
}
