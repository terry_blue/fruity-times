package uk.co.blueshroom.fruitytimes;

import java.util.ArrayList;

import uk.co.blueshroom.fruitytimes.adapters.TableSelectorAdapter;
import uk.co.blueshroom.fruitytimes.ads.LocationValet;
import uk.co.blueshroom.fruitytimes.ads.LocationValet.ILocationValetListener;
import uk.co.blueshroom.fruitytimes.fragments.FragmentMessages;
import uk.co.blueshroom.fruitytimes.fragments.FragmentMessages.Message;
import uk.co.blueshroom.fruitytimes.fragments.FragmentMessages.Message.Tier;
import uk.co.blueshroom.fruitytimes.fragments.TableSelectorFragment;
import uk.co.blueshroom.fruitytimes.objects.BoardsTheme;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.blueshroom.fruitytimes.R;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMRequest;
import com.millennialmedia.android.MMSDK;

public class ActivityTableSelection extends FragmentActivity implements OnClickListener {

	LocationValet locationValet;
	final static String APID = "145163";
	final String TAG = "ActivityTableSelection";
	
	//Constants for tablet sized ads (728x90)
	private static final int IAB_LEADERBOARD_WIDTH = 728;
	private static final int IAB_LEADERBOARD_HEIGHT = 90;

	private static final int MED_BANNER_WIDTH = 480;
	private static final int MED_BANNER_HEIGHT = 60;

	//Constants for phone sized ads (320x50)
	private static final int BANNER_AD_WIDTH = 320;
	private static final int BANNER_AD_HEIGHT = 50;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_table_selection);		
		
		MMSDK.initialize(this);
		
		locationValet = new LocationValet(this, new ILocationValetListener() {
            public void onBetterLocationFound(Location userLocation) {
                MMRequest.setUserLocation(userLocation);
            }
        });
		
		MMAdView adView = (MMAdView)findViewById(R.id.adView);

		//Set your metadata in the MMRequest object
		MMRequest request = new MMRequest();
		//Add the MMRequest object to your MMAdView.
		adView.setMMRequest(request);

		//Sets the id to preserve your ad on configuration changes.
		adView.getAd();
		
		
		
		
		initializeFragments();
		findViewById(R.id.tableselection_imagebutton_settings).setOnClickListener(this);	
		
		
		String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
		Log.e(TAG, "android id = " + androidId);
	}
	
	
	@Override
	public void onResume() {
		if(!locationValet.startAquire(true)) {
            Toast.makeText(this, "Unable to start acquiring location, do you have the permissions declared?", Toast.LENGTH_LONG).show();

            // Manifest.permission.ACCESS_FINE_LOCATION
            // Manifest.permission.ACCESS_COARSE_LOCATION
        }
       
		super.onResume();
	}
	@Override
	public void onPause() {
		locationValet.stopAquire();
		super.onPause();
	}
	
	
	public void initializeFragments() {
		
		//FragmentMessages
		FragmentMessages fragmentMessages = (FragmentMessages)getFragmentManager().findFragmentById(R.id.tableselection_fragment_messages);
		fragmentMessages.addNewMessage(new Message("Tier Low", Tier.LOW, -1));
		fragmentMessages.addNewMessage(new Message("Tier Med", Tier.MED, -1));
		fragmentMessages.addNewMessage(new Message("Tier High", Tier.HIGH, -1));
		
		//Table Selector Fragments
		ArrayList<TableSelectorFragment> tableSelectorFragments = new ArrayList<>();
		tableSelectorFragments.add(TableSelectorFragment.getNewInstance(TableSelectorFragment.THEME_DZ));
		tableSelectorFragments.add(TableSelectorFragment.getNewInstance(TableSelectorFragment.THEME_CARS));
		tableSelectorFragments.add(TableSelectorFragment.getNewInstance(TableSelectorFragment.THEME_THIRD));		
		((ViewPager)findViewById(R.id.tableselection_viewpager_tableselection)).setAdapter(new TableSelectorAdapter(getSupportFragmentManager(), tableSelectorFragments));
	}



	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.tableselection_imagebutton_settings: {
			//FragmentBonus.getInstance().show(getSupportFragmentManager(), FragmentBonus.TAG);
			BoardsTheme.setTHEME(getResources(), BoardsTheme.THEME_ONE);
			startActivity(new Intent(ActivityTableSelection.this, ActivityBoard.class));
			break;
		}
		}
		
	}
	
	
	protected boolean canFit(int adWidth) {
	    int adWidthPx = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, adWidth, getResources().getDisplayMetrics());
	    DisplayMetrics metrics = this.getResources().getDisplayMetrics();
	    return metrics.widthPixels >= adWidthPx;
	}
}
