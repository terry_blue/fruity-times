package uk.co.blueshroom.fruitytimes.views;

import uk.co.blueshroom.fruitytimes.objects.BoardsTheme;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

public class BoardIconIndicator extends FruitIconIndicator {

	public BoardIconIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onDraw(Canvas canvas) {
		if(BoardsTheme.getIconSet() != null) {
			canvas.drawBitmap(BoardsTheme.getIconSet(), srcRect, destRect, null);
		}
	}
}
