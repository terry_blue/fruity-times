package uk.co.blueshroom.fruitytimes.objects;

import android.graphics.Bitmap;

public class DiceIcon extends Icon {

	final static String TAG = "DiceIcon";
	
	public DiceIcon(float ic_type) {
		setIndex(ic_type);
		isSpecial = false;
	}

	

	@Override
	public Bitmap getReelBitmap() {
		return BoardsTheme.getDiceReelBitmap();
	}

	@Override
	public int getReelIconCount() {
		// TODO Auto-generated method stub
		return 12;
	}

	

	@Override
	public Bitmap getSpecialIconBitmap() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
