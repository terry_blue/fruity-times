package uk.co.blueshroom.fruitytimes.objects;

import android.graphics.Bitmap;
import android.graphics.Rect;

public class Interfaces {

	
	public static interface StaticReelIconInterface {
		public Bitmap getReelBitmap();
		public Bitmap getSpecialIconBitmap();
		public int getReelIconCount();
		public Rect getReelIconSrcRect();
	}
	
	public static interface ReelInterface {
		public Bitmap getReelCoverBmp();
		public Rect getReelCoverSrcRect();
		public int getDisplayedIconCount();
	}
	
	
	public static interface DiceReelListener {
		public void onDiceSpinFinished();
	}
}
