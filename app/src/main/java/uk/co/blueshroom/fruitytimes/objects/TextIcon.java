package uk.co.blueshroom.fruitytimes.objects;

import uk.co.blueshroom.fruitytimes.views.FlasherSelector.BonusSelection;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;

public class TextIcon extends Icon {
	
	final String TAG = "TextIcon";
	
	public final String TEXT;
	Rect srcRect;
	public final BonusSelection BONUS;
	public final int REWARD_AMNT;
	
	ObjectAnimator indiFlashAnim;
	
		
	public TextIcon(String text, BonusSelection bonus) {
		TEXT = text;
		srcRect = new Rect();
		BONUS = bonus;
		
		flashAnimator.setDuration(75);
		
		indiFlashAnim = flashAnimator.clone();
		indiFlashAnim.setRepeatCount(5);
		indiFlashAnim.setDuration(800);
		
		
		REWARD_AMNT = (bonus == BonusSelection.EXTRA_CASH) ? ((text.matches("^[0-9]+\\z")) ? Integer.parseInt(text) : 250) : 0;  		
	}		
	
	public void doIdleFlash() {
		indiFlashAnim.start();
	}

	
	@Override
	public void addAnimatorUpdateListener(AnimatorUpdateListener updater) {
		super.addAnimatorUpdateListener(updater);
		indiFlashAnim.addUpdateListener(updater);
	}
	

	@Override
	public Rect getReelIconSrcRect() {
		return srcRect;
	}

	@Override
	public Bitmap getReelBitmap() {
		Paint textPaint = new Paint();
		textPaint.setTextAlign(Align.CENTER);
		textPaint.setColor(0xcc229999);
		textPaint.setShadowLayer(5, 0, 0, 0x882277bb);
		textPaint.setTextSize(20);
		
		int width = (int)(textPaint.measureText(TEXT) + 0.5f);
		float baseline = (-textPaint.ascent() + 0.5f); // ascent() is negative
		int height = (int) (baseline + textPaint.descent() + 0.5f);
		
		Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		srcRect.set(0, 0, width, height);
		Canvas canvas = new Canvas(bmp);
		canvas.drawText(TEXT, width / 2, baseline, textPaint);
	
		return bmp;		
	}

	@Override
	public Bitmap getSpecialIconBitmap() {		
		return null;
	}

	@Override
	public int getReelIconCount() {		
		return 1;
	}

}
