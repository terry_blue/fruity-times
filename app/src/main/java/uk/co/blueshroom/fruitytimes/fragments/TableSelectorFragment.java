package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.ActivityFruitMachine;
import uk.co.blueshroom.fruitytimes.objects.FruitsTheme;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blueshroom.fruitytimes.R;

public class TableSelectorFragment extends Fragment implements OnClickListener {

	public final static String TAG = "TableSelectorFragment";
	
	
	public final static int THEME_DZ = 0;
	public final static int THEME_CARS = 1;
	public final static int THEME_THIRD = 2;
	
	
	public final static String BDL_BACKGROUND_RES_ID = "BDL_BACKGROUND_RES_ID";
	public final static String BDL_THEME_ID = "co.uk.blueshroom.fruitytimes.fragments.BDL_THEME_ID";
	
	
	public static TableSelectorFragment getNewInstance(int THEME_ID) {
		TableSelectorFragment frag = new TableSelectorFragment();
		
		Bundle bdl = new Bundle(1);
		bdl.putInt(BDL_THEME_ID, THEME_ID);		
		
		frag.setArguments(bdl);

		return frag;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tableselector_fragment, container, false);
		
		int bgID = R.drawable.tableselection_icon_dz;
		switch(getArguments().getInt(BDL_THEME_ID, THEME_DZ)) {
		case THEME_DZ: bgID = R.drawable.tableselection_icon_dz; break;
		case THEME_CARS: bgID = R.drawable.tableselection_icon_cars; break;
		case THEME_THIRD: bgID = R.drawable.ic_launcher; break;
		}
		
		((ImageView)view.findViewById(R.id.tableselector_icon_imageview_bg)).setImageResource(bgID);
		
		view.findViewById(R.id.tableselector_icon_relativelayout_root).setOnClickListener(this);
		
		return view;
	}


	@Override
	public void onClick(View view) {
		
		switch(view.getId()) {
		case R.id.tableselector_icon_relativelayout_root: {
			Intent intent = new Intent(getActivity(), ActivityFruitMachine.class);
			FruitsTheme.setTheme(getResources(),  getArguments().getInt(BDL_THEME_ID, THEME_DZ));
			intent.putExtra(BDL_THEME_ID, getArguments().getInt(BDL_THEME_ID, THEME_DZ));
			getActivity().startActivity(intent);
		}
		}
		
	}
}
