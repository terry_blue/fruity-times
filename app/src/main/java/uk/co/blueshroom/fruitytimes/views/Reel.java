package uk.co.blueshroom.fruitytimes.views;

import uk.co.blueshroom.fruitytimes.objects.Icon;
import uk.co.blueshroom.fruitytimes.objects.Interfaces.ReelInterface;
import uk.co.blueshroom.fruitytimes.objects.ObjUtil;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

public abstract class Reel extends View implements AnimatorListener, AnimatorUpdateListener, ReelInterface {

	@SuppressWarnings("unused")
	final private static String TAG = "Reel";
	
	protected boolean isSpinning, isHeld;
	protected Rect drawDestRect, drawSrcRect;
	protected Icon[] displayIcons = new Icon[getDisplayedIconCount()];
	protected Icon[] topReelIcons = new Icon[getDisplayedIconCount()];
	protected Icon[] bottomReelIcons = new Icon[getDisplayedIconCount()];
	final protected static int MIDDLE_IC_COUNT = 4;
	protected Icon[] middleReelIcons = new Icon[MIDDLE_IC_COUNT];
	
	protected Bitmap spinBitmap;
	ObjectAnimator spinAnim, shakeAnim;
	
	float[] cur_display;
	
	public Reel(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		drawDestRect = new Rect();
		drawSrcRect = new Rect();
		
		isSpinning = false;
		isHeld = false;
		
	}

	
	protected void initializeAnimators() {
		if(spinBitmap != null) {
			Rect spinRect1 = new Rect(0, spinBitmap.getHeight(), spinBitmap.getWidth(), spinBitmap.getHeight());
			Rect spinRect2 = new Rect(0, 0, spinBitmap.getWidth(), 0);

			spinAnim = ObjectAnimator.ofObject(this, "drawSrcRect", new ObjUtil.RectEvaluator(), spinRect1, spinRect2);
			spinAnim.setDuration(2500);
			spinAnim.setInterpolator(new AccelerateDecelerateInterpolator());
			spinAnim.addUpdateListener(this);
			spinAnim.addListener(this);

			shakeAnim = ObjectAnimator.ofObject(this, "drawDestRect", new ObjUtil.RectEvaluator(), drawDestRect, new Rect(drawDestRect.left - 5, drawDestRect.top + 5, drawDestRect.right + 5, drawDestRect.bottom - 5));
			shakeAnim.setDuration(100);
			shakeAnim.setInterpolator(new ObjUtil.ShakeInterpolator());
			shakeAnim.addListener(this);
			shakeAnim.addUpdateListener(this);
		}
	}
	
	public void addSpinAnimatorListener(AnimatorListener listener) {
		spinAnim.addListener(listener);
	}	
	
	
	
	
	@Override
	public void onDraw(Canvas canvas) {
		if(isSpinning) {
			canvas.drawBitmap(spinBitmap, drawSrcRect, drawDestRect, null);
		} else {
			for(Icon icon : displayIcons)
				icon.draw(canvas);
		}
		if(getReelCoverBmp() != null) 
			canvas.drawBitmap(getReelCoverBmp(), getReelCoverSrcRect(), drawDestRect, null);
	}
	
	
	public void spin(float[] newReel) {
		cur_display = newReel;
		for(int i = 0; i < newReel.length; i++) {
			bottomReelIcons[i].setIndex(topReelIcons[i].getIndex());
			topReelIcons[i].setIndex(newReel[i]);
			displayIcons[i].setIndex(newReel[i]);
		}
		
		redrawSpinBitmap();
		
		spinAnim.start();
	}
	
	public void redrawSpinBitmap() {
		final int IC_COUNT = topReelIcons.length + middleReelIcons.length + bottomReelIcons.length;
		
		int bmpWidth = topReelIcons[0].getReelIconSrcRect().width();
		int bmpHeight = topReelIcons[0].getReelIconSrcRect().height() * IC_COUNT;

		if(bmpWidth > 0 && bmpHeight > 0) {
			spinBitmap = Bitmap.createBitmap(bmpWidth, bmpHeight, Config.ARGB_8888);


			Canvas canvas = new Canvas(spinBitmap);
			for (int i = 0; i < middleReelIcons.length; i++) {
				middleReelIcons[i].draw(canvas);
				if (i < topReelIcons.length) {
					topReelIcons[i].draw(canvas);
					bottomReelIcons[i].draw(canvas);
				}
			}
		}
	}
	
	
	public void setIconDestRects() { //DestRects are based on the created bitmap, which is created based on drawSrcRect size
		Rect refRect = new Rect(drawSrcRect);
		if(getDisplayedIconCount() > 1)
			refRect.inset(0, drawSrcRect.height() / getDisplayedIconCount());
		
		for(int i = 0; i < MIDDLE_IC_COUNT; i++) {
			middleReelIcons[i].setDestRect(0
										, (refRect.height() * i) + (refRect.height() * topReelIcons.length + 1)
										, refRect.width()
										, (refRect.height() * i) + (refRect.height() * topReelIcons.length) + refRect.height());
			
			if(i < displayIcons.length) {
				displayIcons[i].setDestRect(drawDestRect.left
						, (int)(((float)drawDestRect.height() / (float)getDisplayedIconCount()) * (float)i)
						, drawDestRect.right
						, (int)(((float)drawDestRect.height() / (float)getDisplayedIconCount()) * (float)(i + 1)));
			}
			
			if(i < topReelIcons.length) {
				topReelIcons[i].setDestRect(0
										  , refRect.height() * i
										  , refRect.width()
										  , refRect.height() * (i + 1));
			
			}
			if(i < bottomReelIcons.length)
				bottomReelIcons[i].setDestRect(0
											, (refRect.height() * i) + ((middleReelIcons.length + getDisplayedIconCount()) * refRect.height())
											, refRect.width()
											, refRect.height() + ((refRect.height() * i) + ((middleReelIcons.length + getDisplayedIconCount()) * refRect.height())));
		}
		
	}
	
	
	public void holdReel() {
		isHeld = !isHeld;		
		shakeAnim.start();
		
		//TODO Set hold indicator
	}
	public void unHoldReel() {
		isHeld = false;
	}
	
	public void makeFlash(int pos) {
		displayIcons[pos % displayIcons.length].doFlash();
	}
	
	
	
	
	public boolean getIsHeld() {
		return isHeld;
	}
	public boolean getIsSpinning() {
		return isSpinning;
	}
	
	//Animator set methods
	public void setDrawSrcRect(Rect rect) {
		drawSrcRect = rect;
	}
	public void setDrawDestRect(Rect rect) {
		drawDestRect = rect;
	}
	
	public void setSpinAnimatorStartOffset(long offset) {
		spinAnim.setStartDelay(offset);
	}

}
