package uk.co.blueshroom.fruitytimes.objects;

import java.util.Random;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ArgbEvaluator;
import android.animation.IntEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.blueshroom.fruitytimes.R;

public class BonusSpinner extends View implements AnimatorListener {

	final static String TAG = "BonusSpinner";
	final static int SEGMENT_COUNT = 4;
	public BonusListener bonusListener = null;
	
	public static enum BonusType { LOSE, NO_BONUS, FEATURE, CASH, WINSPIN };
	RectF[] iconDestRects = new RectF[SEGMENT_COUNT];
	Rect[] iconSrcRects = new Rect[BonusType.values().length];
	Bitmap iconsBmp = null;
	
	RectF oval;
	PointF[][] lines = new PointF[SEGMENT_COUNT / 2][2];
	Segment[] segments = new Segment[SEGMENT_COUNT];
	Bonus[] bonuses = new Bonus[SEGMENT_COUNT];
	
	Paint linePaint;
	public boolean isSpinnerStopping, isBonusCompleted;
	ObjectAnimator spinAnim;
	int segPos = 0; int stopPos;
	
	public BonusSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		isBonusCompleted = false;
		oval = new RectF();
		lines[0][0] = new PointF(); lines[0][1] = new PointF();
		lines[1][0] = new PointF(); lines[1][1] = new PointF();
		
		isSpinnerStopping = false;
		
		iconsBmp = BitmapFactory.decodeResource(getResources(), R.drawable.bonus_icons);
		int iconCount = BonusType.values().length;
		for(int i = 0; i < iconCount; i++) {
			iconSrcRects[i] = new Rect(0
									, i * (int)((float)iconsBmp.getHeight() / (float)iconCount)
									, iconsBmp.getWidth()
									, (i + 1) * (int)((float)iconsBmp.getHeight() / (float)iconCount) );
		}
		
		linePaint = new Paint();
		linePaint.setColor(Color.WHITE);
		linePaint.setStrokeWidth(9);
		
		for(int i = 0; i < SEGMENT_COUNT; i++) {
			segments[i] = new Segment((i * 90) - 45);
			iconDestRects[i] = new RectF();
		}
		
		segments[0].light(true);
		
		//set bonuses
		Random rand = new Random();
		bonuses[0] = new Bonus((rand.nextBoolean()) ? BonusType.CASH : BonusType.LOSE, 200);
		bonuses[1] = new Bonus(BonusType.NO_BONUS, 0);
		bonuses[2] = new Bonus((rand.nextBoolean()) ? BonusType.CASH : BonusType.FEATURE, 200);
		bonuses[3] = new Bonus((rand.nextBoolean()) ? BonusType.CASH : BonusType.WINSPIN, 500);
		
	}

	
	
	@Override
	public void onDraw(Canvas canvas) {
		for(Segment segment : segments) {
			segment.draw(canvas);
		}
		
		float[] canvasRotate = new float[] {45, 45};
		for(int i = 0; i < lines.length; i++) {
			canvas.save();
			canvas.rotate(canvasRotate[i], oval.centerX(), oval.centerY());
			canvas.drawLine(lines[i][0].x, lines[i][0].y, lines[i][1].x, lines[i][1].y, linePaint);
			canvas.restore();
		}
		
		for(int i = 0; i < segments.length; i++) {
			canvas.drawBitmap(iconsBmp, iconSrcRects[bonuses[i].getSrcRectIndex()], iconDestRects[i], null);
		}
		
	}
	
	
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int lengthMS = (MeasureSpec.getSize(widthMeasureSpec) > MeasureSpec.getSize(heightMeasureSpec)) ? heightMeasureSpec : widthMeasureSpec;
		super.onMeasure(lengthMS, lengthMS);
		
		oval.set(0, 0, MeasureSpec.getSize(lengthMS), MeasureSpec.getSize(lengthMS));
		
		lines[0][0].set((oval.width() / 2f), 0);
		lines[0][1].set(oval.width() / 2f, oval.height());
		lines[1][0].set(0, oval.height() / 2f);
		lines[1][1].set(oval.width(), oval.height() / 2f);		
		
		iconDestRects[0].set(((oval.width() / 2f) + (oval.width() / 8f) + (oval.width() / 16f))
							, (oval.height() / 2f) - (oval.height() / 8f)
							, ((oval.width() / 2f) + (oval.width() / 4f)) + (oval.width() / 8f) + (oval.width() / 16f)
							, (oval.height() / 2f) + (oval.height() / 8f));
		
		iconDestRects[1].set((oval.width() / 2f) - (oval.width() / 8f)
							, (oval.height() / 2f) + (oval.height() / 8f) + (oval.height() / 16f)
							, (oval.width() / 2f) + (oval.width() / 8f)
							, (oval.height() / 2f) + (oval.width() / 4f) + (oval.width() / 8f) + (oval.height() / 16f));
		
		iconDestRects[2].set((oval.width() / 2f) - (oval.width() / 4f) - (oval.width() / 8f) - (oval.height() / 16f)
							, (oval.height() / 2f) - (oval.height() / 8f)
							, (oval.width() / 2f) - (oval.width() / 8f) - (oval.height() / 16f)
							, (oval.height() / 2f) + (oval.height() / 8f));
		
		iconDestRects[3].set((oval.width() / 2f) - (oval.width() / 8f)
							, (oval.height() / 2f) - (oval.height() / 4f) - (oval.height() / 8f) - (oval.height() / 16f)
							, (oval.width() / 2f) + (oval.height() / 8f)
							, (oval.height() / 2f) - (oval.height() / 8f) - (oval.height() / 16f));
	}
	
	public void onDismiss() {
		if(bonusListener != null)
			bonusListener.onBonusSpinComplete(bonuses[stopPos]);
        else Log.e(TAG, "bonusListener == null");
	}
	
	public void startSpinning() {
		spinAnim = ObjectAnimator.ofObject(this, "segPos", new IntEvaluator(), 0, 1, 2, 3, 4);
		spinAnim.setRepeatCount(ObjectAnimator.INFINITE);
		spinAnim.setDuration(200);
		spinAnim.addListener(this);
		spinAnim.setInterpolator(new LinearInterpolator());
		spinAnim.start();
	}
	public void commenceStopSpinning() {
		if(!isSpinnerStopping) {
			isSpinnerStopping = true;
			stopPos = new Integer(segPos);//Integer.valueOf(segPos);
			
			spinAnim.setDuration(800);
			spinAnim.setRepeatCount(1);
		}
	}
	
	
	public void setBonusListener(BonusListener listener) {
		bonusListener = listener;
	}
	
	//Anim method
	public void setSegPos(int pos) {
		segPos = pos;
		for(int i = 0; i < segments.length; i++) {
			segments[i].light((pos) == i);
		}
		invalidate();
	}
	


	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onAnimationEnd(Animator animation) {
		if(isSpinnerStopping) {
			isSpinnerStopping = false;
			segments[stopPos].startFlashing();
			isBonusCompleted = true;
		}
		
	}



	@Override
	public void onAnimationRepeat(Animator animation) {
		if(isSpinnerStopping) {
			animation.setDuration((long)((float)animation.getDuration() * 2f));
		}
	}



	@Override
	public void onAnimationStart(Animator animation) {
		// TODO Auto-generated method stub
		
	}	
	
	
	
	
	
	public interface BonusListener {
		void onBonusSpinComplete(Bonus bonus);
	}
	
	public class Segment {
		
		Paint paint;
		float startAngle;
		final float SWEEP_ANGLE = 90;
		
		ObjectAnimator flashAnim;
		int paintColor = 0x000000;
		
		
		final static int COLOR_ON = Color.YELLOW;
		final static int COLOR_OFF = Color.BLACK;
		
		public Segment(float start_angle) {
			startAngle = start_angle;
			paint = new Paint();
			
			flashAnim = ObjectAnimator.ofObject(this, "paintColor", new ArgbEvaluator(), COLOR_OFF, COLOR_ON, COLOR_OFF);
			flashAnim.setDuration(800);
			flashAnim.setRepeatCount(ObjectAnimator.INFINITE);
		}
		
		public void draw(Canvas canvas) {
			canvas.drawArc(oval, startAngle, SWEEP_ANGLE, true, paint);
		}
		
		public void light(boolean lightOn) {
			int paintColor = (lightOn) ? COLOR_ON : COLOR_OFF;
			paint.setColor(paintColor);
		}
		
		public void setPaintColor(int col) {
			paintColor = col;
			paint.setColor(paintColor);
			invalidate();
		}
		
		
		public void startFlashing() {
			flashAnim.start();
		}
	}
	
	public static class Bonus {
		final public BonusType bonusType;
		final public int prizeAmount;
		
		public Bonus(BonusType type, int amnt) {
			bonusType = type;
			prizeAmount = amnt;
		}
		
		public int getSrcRectIndex() {
			if(bonusType != null) {
				switch(bonusType) {
				case LOSE: return 1;
				case NO_BONUS: return 2;
				case CASH: return 0;
				case FEATURE: return 3;
				case WINSPIN: return 4;
				}
			}
			return 2;
		}
	}
}
