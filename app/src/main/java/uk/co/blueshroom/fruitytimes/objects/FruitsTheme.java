package uk.co.blueshroom.fruitytimes.objects;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.blueshroom.fruitytimes.R;

public class FruitsTheme {

	final static int THEME_DZ = 0;
	final static int THEME_CARS = 1;
	final static int THEME_FRUITS = 2;
	
	private static Bitmap fruit_reel_bitmap = null;
	private static Bitmap fruit_reel_feature_icon = null;
	private static int fruit_reel_main_background = 0;
	
	
	private static Bitmap fruit_reel_cover = null;
	private static Rect reelCoverSrcRect = null;
	
	
	
	
	
	
	public static void setTheme(Resources res, int THEME) {
		int pos = 0;
		
		int[] reel_bitmap_ids = new int[] { R.drawable.fruitreel_fruits, R.drawable.fruitreel_cars, R.drawable.fruitreel_zombie };
		int[] feature_icon_ids = new int[] { R.drawable.fruitreel_special_icon_fruits, R.drawable.fruitreel_special_icon_cars, R.drawable.fruitreel_special_icon_zombie };
		int[] main_bg_ids = new int[] { R.drawable.fruits_bg_fruits, R.drawable.fruits_bg_cars, R.drawable.fruits_bg_dz };
		
		switch(THEME) {
		case THEME_DZ: pos++;
		case THEME_CARS: pos++;
		case THEME_FRUITS: {
			fruit_reel_bitmap =  BitmapFactory.decodeResource(res, reel_bitmap_ids[pos]);
			fruit_reel_feature_icon = BitmapFactory.decodeResource(res, feature_icon_ids[pos]);
			fruit_reel_main_background = main_bg_ids[pos];
			
			fruit_reel_cover = BitmapFactory.decodeResource(res, R.drawable.reel_cover_1);
			reelCoverSrcRect = new Rect(0, 0, fruit_reel_cover.getWidth(), fruit_reel_cover.getHeight());
		}
		}
	}
	
	public static Bitmap getReelBitmap() {
		return fruit_reel_bitmap;
	}
	
	public static Bitmap getFeatureIcon() {
		return fruit_reel_feature_icon;
	}
	public static int getMainBGResId() {
		return fruit_reel_main_background;
	}
	public static Bitmap getReelCover() {
		return fruit_reel_cover;
	}
	public static Rect getReelCoverSrcRect() {
		return reelCoverSrcRect;
	}
	
	public static int getFruitReelID(int THEME) {
		switch(THEME) {
		case THEME_DZ: return R.drawable.fruitreel_zombie;
		case THEME_CARS: return R.drawable.fruitreel_cars;
		case THEME_FRUITS: return R.drawable.fruitreel_fruits;
		}
		
		return R.drawable.fruitreel_zombie;
	}
	
	public static int getFruitReelSpecialIconID(int THEME) {
		switch(THEME) {
		case THEME_DZ: return R.drawable.fruitreel_special_icon_zombie;
		case THEME_CARS: return R.drawable.fruitreel_special_icon_cars;
		case THEME_FRUITS: return R.drawable.fruitreel_special_icon_fruits;
		}
		
		return R.drawable.fruitreel_special_icon_zombie;
	}
	

	
}
