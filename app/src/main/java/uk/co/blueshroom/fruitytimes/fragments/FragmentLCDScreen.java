package uk.co.blueshroom.fruitytimes.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.blueshroom.fruitytimes.R;

import java.util.ArrayList;
import java.util.Iterator;

import uk.co.blueshroom.fruitytimes.data.Consts;
import uk.co.blueshroom.fruitytimes.objects.Message;
import uk.co.blueshroom.fruitytimes.views.NumberView;


public class FragmentLCDScreen extends Fragment implements Animation.AnimationListener{

    final static String TAG = "FragmentLCDScreen";

    ArrayList<Message> messageList;
    NumberView bankView;
    TextView msgsView;

    public FragmentLCDScreen() {
        // Required empty public constructor
        messageList = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lcdscreen, container, false);
        bankView = (NumberView)v.findViewById(R.id.lcd_numberview_bank);
        msgsView = (TextView)v.findViewById(R.id.lcd_tv_messages);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        bankView.setText(Integer.toString(PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(Consts.SPK_BANK_SCORE, 0)));

    }

    @Override
    public void onStart() {
        super.onStart();
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_messages_anim1);
        animation.setAnimationListener(this);

        msgsView.setAnimation(animation);
    }

    public void addToBank(int amountToAdd) {
        //NumberView numberView = (NumberView)getView().findViewById(R.id.lcd_numberview_bank);

        bankView.addToNumber(amountToAdd);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(Consts.SPK_BANK_SCORE, bankView.getNumber());
        edit.apply();
    }

    int tierPos = 0;
    final Message.Tier[] PRIORITY_PATTERN = new Message.Tier[] { Message.Tier.HIGH, Message.Tier.MED, Message.Tier.HIGH
                                                        , Message.Tier.MED, Message.Tier.HIGH, Message.Tier.LOW };
    public void addNewMessage(Message msg) {
        messageList.add(msg);
    }
    public Message getNextMessage() {
        if(!messageList.isEmpty()) {
            int trapCount = 0;
            while(trapCount < PRIORITY_PATTERN.length + 1) {
                Iterator<Message> it = messageList.iterator();
                while(it.hasNext()) {
                    Message msg = it.next();
                    if(msg.PRIORITY == PRIORITY_PATTERN[tierPos]) {
                        it.remove();
                        if(msg.R_COUNT == -1) addNewMessage(new Message(msg.MESSAGE, msg.PRIORITY, -1));
                        else if(msg.R_COUNT > 1) addNewMessage(new Message(msg.MESSAGE, msg.PRIORITY, msg.R_COUNT-1));

                        if(tierPos < PRIORITY_PATTERN.length - 1) tierPos++; else tierPos = 0;
                        return msg;
                    }
                }

                if(tierPos < PRIORITY_PATTERN.length - 1) tierPos++; else tierPos = 0;
                trapCount++;
            }
        }

        return new Message("HAHA, You Broke Me!", Message.Tier.HIGH, -1);
    }

    @Override
    public void onAnimationStart(Animation anim) {
        msgsView.setText(getNextMessage().MESSAGE);
    }

    @Override
    public void onAnimationEnd(Animation anim) {
        msgsView.findViewById(R.id.lcd_tv_messages);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_messages_anim1);
        animation.setAnimationListener(this);
        msgsView.setText("");
        msgsView.startAnimation(animation);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }




}
