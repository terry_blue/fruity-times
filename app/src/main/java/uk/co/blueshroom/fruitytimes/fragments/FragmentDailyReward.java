package uk.co.blueshroom.fruitytimes.fragments;

import java.lang.ref.SoftReference;
import java.net.InetAddress;
import java.text.DecimalFormat;

import org.apache.commons.net.ntp.NTPUDPClient;

import android.animation.IntEvaluator;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.blueshroom.fruitytimes.R;

public class FragmentDailyReward extends Fragment implements OnClickListener {

	final String TAG = "FragmentDailyReward";
	final static String SP_KEY_NEXT_REWARD = "uk.co.blueshroom.fruitytimes.fragments.dailyreward.nexttime";
	long serverLocalTimeDifference = 0;
	
	DailyRewardHandler dailyRewardHandler;

    Button collectBtn;
    TextView countdownTV;
	
	ObjectAnimator timerAnim;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_daily_reward, container);
	
		dailyRewardHandler = new DailyRewardHandler(this);
		
		timerAnim = ObjectAnimator.ofObject(this, "time_left", new IntEvaluator(), 0, 600000);
		timerAnim.setDuration(600000);
		
		view.findViewById(R.id.dailyreward_button_collect).setOnClickListener(this);
        collectBtn = (Button)view.findViewById(R.id.dailyreward_button_collect);
        countdownTV = (TextView)view.findViewById(R.id.dailyreward_textview_countdown);
		
		return view;
	}
	
	@Override
	public void onStop() {
		super.onStop();
		timerAnim.cancel();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		//((ViewFlipper)getView().findViewById(R.id.dailyreward_viewflipper_root)).setDisplayedChild(0);
		new DailyRewardThread().start();
	}


	@Override
	public void onClick(View v) {
		
		switch(v.getId()) {
		case R.id.dailyreward_button_collect:
			collectBtn.setEnabled(false);
			
			long nextTime = System.currentTimeMillis() + serverLocalTimeDifference + MAX_REWARD_TIME;
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor edit = sp.edit();
			edit.putLong(SP_KEY_NEXT_REWARD, nextTime);
			edit.apply();
			
			updateTimeLeft();
			
			break;
		}
		
	}
	
	
	
	long time_left;
	public void setTime_left(int time) {
		time_left = nextRewardTime - (System.currentTimeMillis() + serverLocalTimeDifference);
		updateTimeLeft();
	}
	
	public void startCountdownTimer(long timeLeft) {	
		time_left = timeLeft;
		//((ViewFlipper)getView().findViewById(R.id.dailyreward_viewflipper_root)).setDisplayedChild(1);
		updateTimeLeft();
		timerAnim.start();
		
	}
	public void updateTimeLeft() {
		if(time_left > 0) {
			long hours = time_left / 3600000l;
			long mins = (time_left / 60000l) % 60;
			long secs = (time_left / 1000l) % 60;
			
			DecimalFormat dF = new DecimalFormat("00");
			
			countdownTV.setText(Long.toString(hours) + " : " + dF.format(mins) + " : " + dF.format(secs));
		} else {
			timerAnim.cancel();
			collectBtn.setEnabled(true);
			countdownTV.setText(R.string.collect);
		}
	}
	
	
	
	
	private static class  DailyRewardHandler extends Handler {
		final SoftReference<FragmentDailyReward> TARGET;
		
		DailyRewardHandler(FragmentDailyReward target) {
			TARGET = new SoftReference<>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			final FragmentDailyReward THIS = TARGET.get();
			if(THIS != null) {
				if(msg.obj instanceof Long) 
					THIS.startCountdownTimer((Long)msg.obj);				
			}
			
		}
	}
	final long MAX_REWARD_TIME = 86400000l;
	long nextRewardTime = 0;
	private class DailyRewardThread extends Thread {
		
		private final String TAG = FragmentDailyReward.this.TAG + "DailyRewardThread";
		
		@Override
		public void run() {
			String[] servers = new String[] {
					"time-a.nist.gov"
					, "time-b.nist.gov"
					, "time-c.nist.gov"
					, "time-d.nist.gov"
					, "nist1-nj.ustiming.org"
					, "nist1-ny2.ustiming.org"
					, "nist1-pa.ustiming.org"					
			};
			
			
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
			nextRewardTime = sp.getLong(SP_KEY_NEXT_REWARD, 0);
			
			for(String server : servers) {
				try { //Send time left to handler	
					
					long serverTime = new NTPUDPClient().getTime(InetAddress.getByName(server)).getMessage().getTransmitTimeStamp().getTime();
					serverLocalTimeDifference = serverTime - System.currentTimeMillis();
					
					if(nextRewardTime == 0) {						
						nextRewardTime = serverTime + serverLocalTimeDifference + MAX_REWARD_TIME;
						SharedPreferences.Editor edit = sp.edit();
						edit.putLong(SP_KEY_NEXT_REWARD, nextRewardTime);
						edit.apply();
					}
					
					
			        Message msg = new Message();
			        msg.obj = nextRewardTime - serverTime; //Time left
			        dailyRewardHandler.sendMessage(msg);
			        break;
				} catch(Exception e) { Log.e(TAG, "getTime error: " + e.toString()); }
			}
		}
	}
	
	
	
}
