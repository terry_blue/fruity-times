package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.views.FruitIconIndicator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blueshroom.fruitytimes.R;

public class FragmentFruitIconIndicator extends IndicatorFragment {

	boolean isCollectionHeld = false;
    ImageView heldIV;
	
	public FragmentFruitIconIndicator() {
		INDICATORS = new FruitIconIndicator[9];
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_fruiticon_indicators, container);
		
		int[] indiIDs = new int[] { R.id.fruiticonindicator_indicator_icon_0, R.id.fruiticonindicator_indicator_icon_1, R.id.fruiticonindicator_indicator_icon_2, R.id.fruiticonindicator_indicator_icon_3, R.id.fruiticonindicator_indicator_icon_4
									, R.id.fruiticonindicator_indicator_icon_5, R.id.fruiticonindicator_indicator_icon_6, R.id.fruiticonindicator_indicator_icon_7, R.id.fruiticonindicator_indicator_icon_8 };
		
		for(int i = 0; i < indiIDs.length; i++)
			INDICATORS[i] = (FruitIconIndicator)view.findViewById(indiIDs[i]);
		
		heldIV = (ImageView)view.findViewById(R.id.fruiticonindicator_imageview_held);

		return view;
	}
	
	
	
	public void holdCollection(boolean hold) {
		int visibility = (hold) ? View.VISIBLE : View.INVISIBLE;
		heldIV.setVisibility(visibility);
		isCollectionHeld = hold;
	}
	
	@Override
	public void allIndicatorsOff() {		
		if(!isCollectionHeld) {
			super.allIndicatorsOff();
			heldIV.setVisibility(View.INVISIBLE);
		}
			
	}
	
	public void addToIndicators(float[][] reel_result) {
        for (float[] aReel_result : reel_result) {
            for (float anAReel_result : aReel_result) {
                if ((int) anAReel_result != 9)
                    setIndicator((int) anAReel_result, true);
            }
        }
		
		//setIsCollectionHeld(new Random().nextInt(10) > 3);
		//DEBUG 
		setIsCollectionHeld(true);
			
		
	}
	
	public void setIsCollectionHeld(boolean held) {
		isCollectionHeld = held;
		int visibility = (held) ? View.VISIBLE : View.INVISIBLE;
		heldIV.setVisibility(visibility);
	}
}
