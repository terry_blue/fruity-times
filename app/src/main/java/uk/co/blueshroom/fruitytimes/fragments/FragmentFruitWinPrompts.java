package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.views.FruitIconIndicator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshroom.fruitytimes.R;

public class FragmentFruitWinPrompts extends IndicatorFragment {

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_win_prompts, container);
		
		INDICATORS = new FruitIconIndicator[10];
		
		int[] winPromptIDs = new int[] { R.id.winprompts_winindicator_0, R.id.winprompts_winindicator_1, R.id.winprompts_winindicator_2, R.id.winprompts_winindicator_3, R.id.winprompts_winindicator_4
										, R.id.winprompts_winindicator_5, R.id.winprompts_winindicator_6, R.id.winprompts_winindicator_7, R.id.winprompts_winindicator_8, R.id.winprompts_winindicator_9 };
		for(int i = 0; i < winPromptIDs.length; i++) 
			INDICATORS[i] = (FruitIconIndicator)view.findViewById(winPromptIDs[i]);
		
		
		return view;
	}
	
	/*
	@Override
	public void onStart() {
		super.onStart();
		
		ViewFlipper vf = (ViewFlipper)getView().findViewById(R.id.winprompts_viewflipper_root);
		vf.setFlipInterval(2000);
		vf.startFlipping();
	}
	*/
		

	public void setPayoutAmounts(int[] payouts) {
		int[] tvIDs = new int[] { R.id.winprompts_textview_win_0, R.id.winprompts_textview_win_1, R.id.winprompts_textview_win_2, R.id.winprompts_textview_win_3, R.id.winprompts_textview_win_4
								, R.id.winprompts_textview_win_5, R.id.winprompts_textview_win_6, R.id.winprompts_textview_win_7, R.id.winprompts_textview_win_8, R.id.winprompts_textview_win_9 };
		
		for(int i = 0; i < tvIDs.length; i++) {
			((TextView)getView().findViewById(tvIDs[i])).setText(Integer.toString(payouts[i]));
		}
	}
}
