package uk.co.blueshroom.fruitytimes.objects;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.blueshroom.fruitytimes.R;

public class BoardsTheme {

	public final static int THEME_ONE = 0;
	public final static int THEME_TWO = 1;
	public final static int THEME_THREE = 2;
	
	private static Bitmap icon_set;
	public final static int ICON_COUNT = 9;
	
	private static Bitmap dice_reel_cover;
	private static Bitmap dice_reel_ic;
	private static Rect dice_reel_cover_rect;
	
	public static void setTHEME(Resources res, int THEME) {
		
		switch(THEME) {
		case THEME_ONE:
		case THEME_TWO:
		case THEME_THREE:
			icon_set = BitmapFactory.decodeResource(res, R.drawable.icon_set_board_a);
			dice_reel_ic = BitmapFactory.decodeResource(res, R.drawable.dice_reel_1);
			dice_reel_cover = BitmapFactory.decodeResource(res, R.drawable.die_cover);
			
			dice_reel_cover_rect = new Rect(0, 0, dice_reel_cover.getWidth(), dice_reel_cover.getHeight());
		}
	}
	
	
	
	public static Bitmap getIconSet() {
		return icon_set;
	}
	
	public static Bitmap getDiceReelBitmap() {
		return dice_reel_ic;
	}
	public static Bitmap getDiceReelCoverBitmap() {
		return dice_reel_cover;
	}
	public static Rect getDiceReelCoverSrcRect() {
		return dice_reel_cover_rect;
	}
}
