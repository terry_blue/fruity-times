package uk.co.blueshroom.fruitytimes.views;

import uk.co.blueshroom.fruitytimes.objects.FruitsTheme;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class FruitIconIndicator extends View {

	final static String TAG = "FruitIconIndicator";
	
	Paint paint = null;
	Rect destRect, srcRect;
	public boolean isEnabled;
	
	
	public FruitIconIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		destRect = new Rect();
		srcRect = new Rect();
		paint = new Paint();		
		
	}

	
	@Override
	public void onDraw(Canvas canvas) {				
		if(FruitsTheme.getReelBitmap() != null)  
			canvas.drawBitmap(FruitsTheme.getReelBitmap(), srcRect, destRect, paint);
		
	}
	
	@Override
	public void onMeasure(int wMeasureSpec, int hMeasureSpec) {		
		super.onMeasure(MeasureSpec.makeMeasureSpec(srcRect.width(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(srcRect.height(), MeasureSpec.AT_MOST));
		destRect.set(0, 0,srcRect.width(), srcRect.height());
	}
	
	@SuppressLint("WrongCall") //Method called from child object as to avoid the call to above onMeasure()
	protected void doMeasure(int wMeaureSpec, int hMeasureSpec) {
		super.onMeasure(wMeaureSpec, hMeasureSpec);
	}
		
	
	public void setSrcRect(int left, int top, int right, int bottom) {
		srcRect.set(left, top, right, bottom);
	}
	public void setSrcRect(Rect rect) {
		if(rect != null)
			srcRect.set(rect);
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		isEnabled = enabled;
		int color = (enabled) ? Color.argb(0, 200, 200, 200) : Color.argb(210, 200, 200, 200);
		PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);
		paint.setColorFilter(colorFilter);
		invalidate();
	}
}
