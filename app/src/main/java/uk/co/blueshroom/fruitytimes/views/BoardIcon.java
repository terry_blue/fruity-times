package uk.co.blueshroom.fruitytimes.views;

import uk.co.blueshroom.fruitytimes.fragments.FragmentBoardIcons.IconType;
import uk.co.blueshroom.fruitytimes.objects.BoardsTheme;
import android.animation.Animator.AnimatorListener;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

public class BoardIcon extends View {

	Rect drawRect, srcRect;
	int lightColor = 0x00000000;
	Paint lightPaint;
	
	final public static int IC_MULTI_ADD = 0;
	final public static int IC_ADD_CASH = 1;
	final public static int IC_ADD_HITS = 2;
	final public static int IC_LOSE = 3;
	final public static int IC_CHANCE = 4;
	final public static int IC_START = 5;
	final public static int IC_XTRA_LIFE = 6;
	final public static int IC_HITS_IC = 7;
	final public static int IC_BONUS_IC = 8;
	
	//int curIcon = 0;
	public IconType ic_type;
	
	ObjectAnimator pingAnimator, flashAnimator;
	
	public BoardIcon(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		drawRect = new Rect();
		srcRect = new Rect();
		lightPaint = new Paint();
		lightPaint.setColor(Color.TRANSPARENT);
		
		pingAnimator = ObjectAnimator.ofObject(this, "lightColor", new ArgbEvaluator(), Color.TRANSPARENT, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.TRANSPARENT);
		pingAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
		pingAnimator.setDuration(400);
		
		flashAnimator = ObjectAnimator.ofObject(this, "lightColor", new ArgbEvaluator(), Color.TRANSPARENT, Color.YELLOW, Color.WHITE, Color.TRANSPARENT);
		flashAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
		flashAnimator.setRepeatCount(ObjectAnimator.INFINITE);
		flashAnimator.setDuration(1000);
	}
	
	@Override
	public void onDraw(Canvas canvas) {		
		
		canvas.drawRect(drawRect, lightPaint);
		
		if(BoardsTheme.getIconSet() != null) {
			canvas.drawBitmap(BoardsTheme.getIconSet(), srcRect, drawRect, null);
		}
	}
	
	public void setIcon(IconType IC_TYPE) {
		ic_type = IC_TYPE;
		srcRect.set(getSrcRect(IC_TYPE.ordinal()));
		invalidate();
	}
	public static Rect getSrcRect(final int INDEX) {
		if(BoardsTheme.getIconSet() != null) {
			int height = (int)((float)BoardsTheme.getIconSet().getHeight() / (float)BoardsTheme.ICON_COUNT);
			return new Rect(0, INDEX * height, BoardsTheme.getIconSet().getWidth(), (INDEX + 1) * height);
		}
		
		return null;
	}
	
	
	public void setLightColor(int color) {
		lightPaint.setColor(color);
		invalidate();
	}
	
	public void doIdleFlash() {
		flashAnimator.start();
	}
	public void stopIdleFlash() {
		flashAnimator.end();
	}
	public void doPing() {
		pingAnimator.start();
	}
	public void setAnimatorListener(AnimatorListener listener) {
		pingAnimator.addListener(listener);
		
	}
	
	static int length = -1;
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if(length == -1)
			length = MeasureSpec.getSize(heightMeasureSpec) / 5;
		
		super.onMeasure(MeasureSpec.makeMeasureSpec(length, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(length, MeasureSpec.EXACTLY));
		
		drawRect.set(0, 0, length, length);
		drawRect.inset(2, 2);
	}
	
	
	
	public static Bitmap getIcon(int INDEX) {
		Bitmap bmp = Bitmap.createBitmap(getSrcRect(INDEX).width(), getSrcRect(INDEX).height(), Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(bmp);
		c.drawBitmap(BoardsTheme.getIconSet(), getSrcRect(INDEX), new Rect(0, 0, c.getWidth(), c.getHeight()), null);
		return bmp;
	}
	
}
