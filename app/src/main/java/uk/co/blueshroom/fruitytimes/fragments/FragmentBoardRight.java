package uk.co.blueshroom.fruitytimes.fragments;

import java.util.Arrays;

import uk.co.blueshroom.fruitytimes.fragments.FragmentBoardIcons.IconType;
import uk.co.blueshroom.fruitytimes.objects.TextIcon;
import uk.co.blueshroom.fruitytimes.views.BoardIcon;
import uk.co.blueshroom.fruitytimes.views.BoardIconIndicator;
import uk.co.blueshroom.fruitytimes.views.FeatureIndicatorGroup;
import uk.co.blueshroom.fruitytimes.views.FlasherSelector;
import uk.co.blueshroom.fruitytimes.views.FlasherSelector.BonusSelection;
import uk.co.blueshroom.fruitytimes.views.NumberView;
import uk.co.blueshroom.fruitytimes.views.NumberView.NumberEmptiedListener;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.blueshroom.fruitytimes.R;

public class FragmentBoardRight extends Fragment implements NumberEmptiedListener {

	
	final static String TAG = "FragmentBoardRight";
	
	final static int MAX_INDICATORS = 3;
	BoardIconIndicator[] bi_hits = new BoardIconIndicator[MAX_INDICATORS];
	BoardIconIndicator[] bi_xtra = new BoardIconIndicator[MAX_INDICATORS];
	BoardIconIndicator[] bi_bonus = new BoardIconIndicator[MAX_INDICATORS];
	
	FeatureIndicatorGroup figHits;
	FeatureIndicatorGroup figXtra;
	FeatureIndicatorGroup figBonus;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_board_right, container);
		
		figHits = (FeatureIndicatorGroup)view.findViewById(R.id.boardright_indicatorgroup_group1);
		figXtra = (FeatureIndicatorGroup)view.findViewById(R.id.boardright_indicatorgroup_group3);
		figBonus = (FeatureIndicatorGroup)view.findViewById(R.id.boardright_indicatorgroup_group2);		
		
		figHits.setIconBitmap(BoardIcon.getIcon(BoardIcon.IC_HITS_IC));
		figXtra.setIconBitmap(BoardIcon.getIcon(BoardIcon.IC_XTRA_LIFE));
		figBonus.setIconBitmap(BoardIcon.getIcon(BoardIcon.IC_BONUS_IC));
		
		figHits.setTitle("Hits");
		figXtra.setTitle("Xtra Life");
		figBonus.setTitle("Bonus Game");		
		
		
		String[] hitsFlashers = new String[] { "1", "2", "5", "10", "15", "20", "50", "100" };
		BonusSelection[] hitBonusSelections = new BonusSelection[] { BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_CASH };
		String[] bonusFlashers = new String[] { "Random Icon", "Extra Cash", "Extra Hits", "Extra Life", "Bonus Game" };
		BonusSelection[] bonusBonusSelections = new BonusSelection[] { BonusSelection.RANDOM_ICON, BonusSelection.EXTRA_CASH, BonusSelection.EXTRA_HITS, BonusSelection.XTRA_LIFE, BonusSelection.BONUS_GAME };
		String[] chanceFlashers = new String[] { "Lose", "Hi-Lo to Go", "Extra Life", "Bonus Game", "Game Over" };
		BonusSelection[] chanceBonusSelections = new BonusSelection[] { BonusSelection.LOSE, BonusSelection.HI_LOW, BonusSelection.XTRA_LIFE, BonusSelection.BONUS_GAME, BonusSelection.GAME_OVER };
		
		((FlasherSelector)view.findViewById(R.id.boardright_flashselector_group1)).addChildren(Arrays.asList(hitsFlashers), Arrays.asList(hitBonusSelections));
		((FlasherSelector)view.findViewById(R.id.boardright_flashselector_group2)).addChildren(Arrays.asList(bonusFlashers), Arrays.asList(bonusBonusSelections));
		((FlasherSelector)view.findViewById(R.id.boardright_flashselector_group3)).addChildren(Arrays.asList(chanceFlashers), Arrays.asList(chanceBonusSelections));
		
		((NumberView)view.findViewById(R.id.boardright_numberview_cashpot)).addEmptyListener(this);
		
		
		
		
		
		return view;
	}
	
	public void setOnClickListener(OnClickListener listener) {
		getView().findViewById(R.id.boardright_button_collect).setOnClickListener(listener);
		getView().findViewById(R.id.boardright_button_spin).setOnClickListener(listener);
	}
	
	
	public boolean addToIndicators(IconType ic) {
		switch(ic) {
		case IC_HITS: 
			return (figHits.addToIcons());		
		case IC_BONUS: 
			return (figBonus.addToIcons());		
		case XTRA: 
			return (figXtra.addToIcons());		
		}
		
		return false;
	}
	public boolean isIconCollectionComplete(IconType ic) {
		switch(ic) {
		case IC_HITS: return figHits.isCollectionComplete();
		case IC_BONUS: return figBonus.isCollectionComplete();
		case XTRA: return figXtra.isCollectionComplete();
		}
		
		return false;
	}
	
	public void clearBonus() {
		figBonus.clearAllIcons();
	}
	public void addExtraLife() {
		while(!addToIndicators(IconType.XTRA)) { }
		//getView().findViewById(R.id.boardright_textview_xtralife).setVisibility(View.VISIBLE);
	}
	public void useExtraLife() {
		figXtra.clearAllIcons();
		//getView().findViewById(R.id.boardright_textview_xtralife).setVisibility(View.INVISIBLE);
	}
	
	
	public void startFlasherSelector(int id) {
        if(getView() != null)
		    ((FlasherSelector)getView().findViewById(id)).startRandomFlashing();
	}
	
	
	public TextIcon getBonusFromFlashing() {
		
		FlasherSelector hitsSelector = ((FlasherSelector)getView().findViewById(R.id.boardright_flashselector_group1));
		FlasherSelector bonusSelector = ((FlasherSelector)getView().findViewById(R.id.boardright_flashselector_group2));
		FlasherSelector chanceSelector = ((FlasherSelector)getView().findViewById(R.id.boardright_flashselector_group3));
		
		if(hitsSelector.getIsFlashing())
			return hitsSelector.getSelectedTextIcon();
		
		if(bonusSelector.getIsFlashing())
			return bonusSelector.getSelectedTextIcon();
		
		if(chanceSelector.getIsFlashing())
			return chanceSelector.getSelectedTextIcon();
		
		return null;
	}

	
	
	@Override
	public void onNumberEmptied() { //Called when the cashpot is fully empty (ie after user "collects"
		getActivity().onBackPressed();
		
	}
}
