package uk.co.blueshroom.fruitytimes.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.AttributeSet;

/**
 * Created by Terry on 10/01/2016.
 */
public class BankView extends NumberView {

    final static String SP_KEY_BANK_SCORE = "uk.co.blueshroom.fruitytimes.views.bankview.score";

    public BankView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        startNumber(sp.getInt(SP_KEY_BANK_SCORE, 0));
    }

    public void addToBank(int amountToAdd) {
        addToNumber(amountToAdd);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(SP_KEY_BANK_SCORE, getNumber());
        edit.apply();
    }
}
