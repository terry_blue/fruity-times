package uk.co.blueshroom.fruitytimes.objects;

import android.graphics.Bitmap;

public class FruitReelIcon extends Icon {
	
	final static String TAG = "FruitReelIcon";	
	
	public FruitReelIcon(float ic_type) {			
		setIndex(ic_type);		
	}
	
	@Override
	public void setIndex(float newIndex) {
		super.setIndex(newIndex);
		isSpecial = (newIndex * 10) % 10 != 0;
	}	


	@Override
	public Bitmap getReelBitmap() {
		return FruitsTheme.getReelBitmap();
	}


	@Override
	public int getReelIconCount() {
		return 10;
	}
	

	@Override
	public Bitmap getSpecialIconBitmap() {
		return FruitsTheme.getFeatureIcon();
	}
}