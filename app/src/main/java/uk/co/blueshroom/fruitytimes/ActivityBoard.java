package uk.co.blueshroom.fruitytimes;

import java.util.Random;

import uk.co.blueshroom.fruitytimes.fragments.FragmentBank;
import uk.co.blueshroom.fruitytimes.fragments.FragmentBoardIcons;
import uk.co.blueshroom.fruitytimes.fragments.FragmentBoardIcons.IconType;
import uk.co.blueshroom.fruitytimes.fragments.FragmentBoardIcons.LandingListener;
import uk.co.blueshroom.fruitytimes.fragments.FragmentBoardRight;
import uk.co.blueshroom.fruitytimes.objects.TextIcon;
import uk.co.blueshroom.fruitytimes.views.NumberView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.blueshroom.fruitytimes.R;

public class ActivityBoard extends Activity implements OnClickListener, LandingListener {

	
	boolean extraLife = false;
	
	public final static int BONUS_GAME_REQUEST = 21;
	public final static String BK_AMNT_TO_ADD = "uk.co.blueshroom.fruitytimes.ActivityBoard.amount_to_add_to_bank";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_board);		
		
		((FragmentBoardIcons)getFragmentManager().findFragmentById(R.id.board_fragment_boardicons)).setLandingListener(this);
		
	}
	
	@Override
	public void onStart() {
		super.onStart();
		((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		switch(v.getId()) {
		case R.id.boardright_button_collect: {
			NumberView cashpotNV = (NumberView)findViewById(R.id.boardright_numberview_cashpot);
			int cashpotValue = cashpotNV.getNumber();
			cashpotNV.emptyNumber();
			
			((FragmentBank)getFragmentManager().findFragmentById(R.id.boardright_fragment_bank)).addToBank(cashpotValue);
			
			break; 
		}
		case R.id.boardright_button_spin: {
			TextIcon reward = ((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).getBonusFromFlashing();
			if(reward != null) 
				doTextIconReward(reward);
			else 
				((FragmentBoardIcons)getFragmentManager().findFragmentById(R.id.board_fragment_boardicons)).spinDice();
			
			break;
		}
			
		}
		
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
		case BONUS_GAME_REQUEST:
			if(resultCode == RESULT_OK) {
				int resultValue = data.getIntExtra(BK_AMNT_TO_ADD, 0);
				((FragmentBank)getFragmentManager().findFragmentById(R.id.boardright_fragment_bank)).addToBank(resultValue);
			}
			break;
		}
	}

	@Override
	public void onIconLand(IconType icon) {
		
		switch(icon) {
		case MULTI_ADD: 
			IconType[] multis = new IconType[] { IconType.ADD_CASH, IconType.XTRA, IconType.IC_BONUS, IconType.IC_HITS };
			int multiCount = new Random().nextInt(multis.length) + 1;
			
			for(int i = 0; i < multiCount; i++) 
				onIconLand(multis[new Random().nextInt(multis.length)]);
			
			break;		
		case ADD_CASH: 
			((NumberView)findViewById(R.id.boardright_numberview_cashpot)).addToNumber(200);
			break;		
		case ADD_HITS: 
			if(((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).isIconCollectionComplete(IconType.IC_HITS))
				((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).startFlasherSelector(R.id.boardright_flashselector_group1);
			break;		
		case LOSE:
			doLose();
			break;		
		case CHANCE:
			((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).startFlasherSelector(R.id.boardright_flashselector_group3);
			break;		
		case START: 			
			((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).startFlasherSelector(R.id.boardright_flashselector_group2);
			break;		
		case XTRA: 
			if(((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).addToIndicators(icon))
				addExtraLife();			
			break;		
		case IC_HITS: 
			if(((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).addToIndicators(icon))
				((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).startFlasherSelector(R.id.boardright_flashselector_group1);
			break;	
		case IC_BONUS: 
			if(((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).addToIndicators(icon))
				startBonusGame();			
			break;		
		}
	}
	
	
	public void doTextIconReward(TextIcon bonus) {
		switch(bonus.BONUS) {
		case GAME_OVER:
		case LOSE:
			doLose();
			break;
		case HI_LOW:
			break;
		case XTRA_LIFE:
			addExtraLife();
			break;
		case RANDOM_ICON:
			IconType[] icons = new IconType[] { IconType.XTRA, IconType.IC_HITS, IconType.IC_BONUS, IconType.IC_HITS };
			((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).addToIndicators(icons[new Random().nextInt(icons.length)]);
			break;
		case EXTRA_CASH:
			((NumberView)findViewById(R.id.boardright_numberview_cashpot)).addToNumber(bonus.REWARD_AMNT);
			break;
		case BONUS_GAME:
			startBonusGame(); 
			break;
		case EXTRA_HITS:
			((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).addToIndicators(IconType.IC_HITS);
			break;
		}
	}
	
	public void startBonusGame() {		
		((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).clearBonus();
		startActivityForResult(new Intent(this, ActivityBonusGame.class), BONUS_GAME_REQUEST);
	}
	
	public void addExtraLife() {				
		((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).addExtraLife();
		
		extraLife = true;		
	}
	public void doLose() {
		if(extraLife) {
			extraLife = false;
			((FragmentBoardRight)getFragmentManager().findFragmentById(R.id.board_fragment_boardright)).useExtraLife();
		} else {
			//WAAAM WAAAM WAAAAAAMMMMPPPPPPP
			//super.onBackPressed();			
		}
		

	}
}
