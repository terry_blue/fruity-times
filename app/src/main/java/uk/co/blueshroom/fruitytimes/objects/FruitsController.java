package uk.co.blueshroom.fruitytimes.objects;

import java.util.Random;

import uk.co.blueshroom.fruitytimes.data.Spins_DB.WinType;

public class FruitsController {
	
	final static String TAG = "FruitsController";
	
	private int curStakePos = 0;
	
	public final static float[] STAKES = new float[] { 1f, 2f, 5f, 10f, 20f, 50f, 100f };
	public final static float[] PAYOUTS = new float[] { 1.5f, 2f, 2.5f, 3f, 4f, 5f, 6f, 8f, 10f, 20f };
	
	
	public void changeStake() {
		if(curStakePos < STAKES.length - 1)
			curStakePos++;
		else curStakePos = 0;
	}
	public float getStake() {
		return STAKES[curStakePos];
	}
	public int[] getAdjPayouts() {
		int[] adj = new int[PAYOUTS.length];
		for(int i = 0; i < adj.length; i++) 
			adj[i] = (int)(PAYOUTS[i] * getStake());
			
		return adj;
	}
	
	public boolean shouldEnableHold() {
		return new Random().nextInt(10) > 5;
	}
	
	public int getWinAmount(int index) {
		return (int)(STAKES[curStakePos] * PAYOUTS[index]);
	}
	
	public static WinType getWinType() {
		Random rand = new Random();
		if(rand.nextInt(10) > 5) {
			if(rand.nextInt(10) > 5) {
				if(rand.nextInt(10) > 5) {
					if(rand.nextInt(10) > 5) {
						if(rand.nextInt(10) > 7)
							return WinType.RARE;
						else return WinType.FEATURE;
					} else return WinType.MED;
				} else return WinType.LOW;
			} else return WinType.BLANK;				
		} else return WinType.BLANK;
	}
	
	public static WinType getWinSpinType() {
		Random rand = new Random();
		if(rand.nextInt(10) > 5) {
			if(rand.nextInt(10) > 6) {
				return WinType.RARE;
			} else return WinType.MED;
		} else return WinType.LOW;
	}
}
