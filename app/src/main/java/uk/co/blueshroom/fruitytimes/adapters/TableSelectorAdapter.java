package uk.co.blueshroom.fruitytimes.adapters;

import java.util.ArrayList;

import uk.co.blueshroom.fruitytimes.fragments.TableSelectorFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TableSelectorAdapter extends FragmentPagerAdapter {

	ArrayList<TableSelectorFragment> fragments;
	
	public TableSelectorAdapter(FragmentManager fragmentManager, ArrayList<TableSelectorFragment> tableSelectors) {
		super(fragmentManager);
		this.fragments = tableSelectors;
	}
	
	@Override
	public int getCount() {
		return fragments.size();
	}

	

	@Override
	public Fragment getItem(int i) {
		if(fragments != null) 
			return fragments.get(i);
		
		return null;
	}

}
