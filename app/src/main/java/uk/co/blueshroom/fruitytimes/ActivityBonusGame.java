package uk.co.blueshroom.fruitytimes;

import com.blueshroom.fruitytimes.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class ActivityBonusGame extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bonus_game);
		
		Intent resultIntent = new Intent();
		resultIntent.putExtra(ActivityBoard.BK_AMNT_TO_ADD, 225);
		setResult(RESULT_OK, resultIntent);
	}
}
