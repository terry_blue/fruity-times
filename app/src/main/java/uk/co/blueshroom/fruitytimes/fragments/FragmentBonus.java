package uk.co.blueshroom.fruitytimes.fragments;

import uk.co.blueshroom.fruitytimes.objects.BonusSpinner;
import uk.co.blueshroom.fruitytimes.objects.BonusSpinner.BonusListener;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.blueshroom.fruitytimes.R;

public class FragmentBonus extends DialogFragment implements OnClickListener {

	final public static String TAG = "FragmentBonus";
    BonusSpinner bonusSpinner;
	BonusListener bonusListener;
	static FragmentBonus instance;
	
	public static FragmentBonus getInstance() {
		if(instance == null)
			instance = new FragmentBonus();
		
		return instance;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_bonus, container);
        bonusSpinner = ((BonusSpinner)view.findViewById(R.id.bonus_bonusspinner_getbonus));
		bonusSpinner.startSpinning();
		bonusSpinner.setOnClickListener(this);

        if(getActivity() instanceof BonusListener) {
            bonusListener = (BonusListener)getActivity();
            bonusSpinner.setBonusListener(bonusListener);
        } else {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }

		setCancelable(false);
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_base);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		bonusSpinner.onDismiss();
		
		getFragmentManager().beginTransaction().remove(this).commit();
	}


	@Override
	public void onClick(View v) {

		switch(v.getId()) {
		case R.id.bonus_bonusspinner_getbonus: {
			if(bonusSpinner.isBonusCompleted)
				dismiss();
			else 
				bonusSpinner.commenceStopSpinning();
		}
		}
		
	}
}
