package uk.co.blueshroom.fruitytimes.views;

import uk.co.blueshroom.fruitytimes.objects.FruitsTheme;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

public class FeatureIconIndicator extends FruitIconIndicator {



    public FeatureIconIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onDraw(Canvas canvas) {
        if(FruitsTheme.getFeatureIcon() != null) {
            canvas.drawBitmap(FruitsTheme.getFeatureIcon(), srcRect, destRect, paint);
        }
    }
}