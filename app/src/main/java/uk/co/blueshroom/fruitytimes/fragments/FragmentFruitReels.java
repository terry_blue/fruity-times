package uk.co.blueshroom.fruitytimes.fragments;

import java.util.ArrayList;
import java.util.Random;

import uk.co.blueshroom.fruitytimes.data.Spins_DB;
import uk.co.blueshroom.fruitytimes.data.Spins_DB.WinType;
import uk.co.blueshroom.fruitytimes.objects.FruitReel;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blueshroom.fruitytimes.R;

public class FragmentFruitReels extends Fragment implements AnimatorListener {

	int spinCount = 0;

    final static String TAG = "FragmetnFruitReels";

	OnSpinListener mListener;
	//SpinListener spinListener;
	//WinListener winListener;
	
	final FruitReel[] FRUIT_REELS = new FruitReel[3];
	float[][] displayed_reels = new float[3][3];
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_reels, container);
		
		FRUIT_REELS[0] = (FruitReel)view.findViewById(R.id.fruitReel1);
		FRUIT_REELS[1] = (FruitReel)view.findViewById(R.id.fruitReel2);
		FRUIT_REELS[2] = (FruitReel)view.findViewById(R.id.fruitReel3);
		for(int i = 0; i < FRUIT_REELS.length; i++) {
			FRUIT_REELS[i].addSpinAnimatorListener(this);
			FRUIT_REELS[i].setSpinAnimatorStartOffset((long)(i * 240));
		}
		
		
		return view;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		try {
			mListener = (OnSpinListener)(context);
            Log.e(TAG, "successfull cast");
		} catch(ClassCastException e) { throw new ClassCastException(context.toString()
											+ "must implement FragmentFruitReels.OnSpinListener");
		}
	}

    @SuppressWarnings("deprecation")
    @Override //Method for API < 23
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSpinListener)(activity);
            Log.e(TAG, "successfull cast");
        } catch(ClassCastException e) { throw new ClassCastException(activity.toString()
                + "must implement FragmentFruitReels.OnSpinListener");
        }
    }


	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	
	public float[][] spinReels(WinType winResult) {
		if(!isSpinning()) {
			spinCount++;
			Spins_DB db = new Spins_DB(getActivity());
			float[][] reelSpin = db.getReelSpin(winResult);
			db.close();
			for(int i = 0; i < FRUIT_REELS.length; i++) {
				if(!FRUIT_REELS[i].getIsHeld()) {					
					FRUIT_REELS[i].spin(reelSpin[i]);
					displayed_reels[i] = reelSpin[i];
				}
				FRUIT_REELS[i].unHoldReel();
			}
			
		}
		return displayed_reels;
	}
	
	public boolean isSpinning() {		
		for(FruitReel reel : FRUIT_REELS) {
			if(reel.getIsSpinning())
				return true;
		}
		
		return false;
	}
	
	public void holdReel(int REEL) {
		FRUIT_REELS[REEL].holdReel();
	}
	
	public float[] getRandomReel() {
		float[] array = new float[3];
		for(int i = 0; i < 3; i++) {
			array[i] = (float)(new Random().nextInt(9));
			array[i] += (new Random().nextInt(10) > 7) ? 0.1f : 0;
		}
		
		return array;
	}
	
	

	

	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationEnd(Animator animation) {
		if(!isSpinning()) {
			mListener.onSpinFinished(displayed_reels);

			//TODO determine win
			WinObject win = WinObject.getWin(displayed_reels);
			if(win != null) {
				for(int i = 0; i < FRUIT_REELS.length; i++) 
					FRUIT_REELS[i].makeFlash(win.posOfWinIcons[i]);
				mListener.onWin(win);
			}
			
			
			
			
		}
		
	}

	@Override
	public void onAnimationRepeat(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animator animation) {
		// TODO Auto-generated method stub
		
	}
	
	public static class WinObject {
		final public int[] posOfWinIcons;
		final public int winIndex;
		public WinObject(int[] posOfWinIcons, int winIndex){ 
			this.posOfWinIcons = posOfWinIcons;
			this.winIndex = winIndex;
		}
		
		public static WinObject getWin(float[][] reels) {
			ArrayList<WinObject> wins = new ArrayList<>();
			
			int[] line1 = new int[] { 0,0,0 };
			int[] line2 = new int[] { 1,1,1 };
			int[] line3 = new int[] { 2,2,2 };
			int[] line4 = new int[] { 0,1,2 };
			int[] line5 = new int[] { 2,1,0 };
			int[][] winLines = new int[][] { line1, line2, line3, line4, line5 };
			
			for(int[] line : winLines) {
				if((int)reels[0][line[0]] == (int)reels[1][line[1]] && (int)reels[0][line[0]] == (int)reels[2][line[2]]) {
					wins.add(new WinObject(line, (int)reels[0][line[0]]));
				}
			}


			WinObject win = null;
			for(WinObject w : wins) {
				if(win == null)
					win = w;
				else if(w.winIndex > win.winIndex) 
					win = w;					
			}			
			
			return win;
		}
	}
	
	



	public interface OnSpinListener {
		void onSpinFinished(float[][] spin_result);
		void onWin(WinObject win);
	}
}
