package uk.co.blueshroom.fruitytimes.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;

import uk.co.blueshroom.fruitytimes.objects.FruitsTheme;

public class FruitIconWinIndicator extends FruitIconIndicator {

    Rect destRect2, destRect3;

    public FruitIconWinIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        destRect2 = new Rect();
        destRect3 = new Rect();
    }


    @Override
    public void onDraw(Canvas canvas) {
        //destRect.set((int)((float)getWidth() * 0.1f), (int)((float)getHeight() * 0.1f), (int)((float)getWidth() * 0.8f), (int)((float)getHeight() * 0.9));
        //destRect2.set(destRect.left, destRect.top, destRect.right, destRect.bottom);
        //destRect3.set(destRect.left, destRect.top, destRect.right, destRect.bottom);
        //destRect2.offset(WIN_ICON_GAP, 0);
        //destRect3.offset(WIN_ICON_GAP * 2,  0);
        if(FruitsTheme.getReelBitmap() != null) {
            canvas.drawBitmap(FruitsTheme.getReelBitmap(), srcRect, destRect, paint);
            canvas.drawBitmap(FruitsTheme.getReelBitmap(), srcRect, destRect2, paint);
            canvas.drawBitmap(FruitsTheme.getReelBitmap(), srcRect, destRect3, paint);
        }
    }

    final static int WIN_ICON_GAP = 25;
    @Override
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {

        destRect.set(0, 0, srcRect.right, srcRect.bottom);
        destRect2.set(destRect.left, destRect.top, destRect.right, destRect.bottom);
        destRect3.set(destRect.left, destRect.top, destRect.right, destRect.bottom);


        destRect2.offset(WIN_ICON_GAP, 0);
        destRect3.offset(WIN_ICON_GAP * 2,  0);

        doMeasure(MeasureSpec.makeMeasureSpec(destRect3.right - destRect.left, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(destRect.height(), MeasureSpec.AT_MOST));
    }
}