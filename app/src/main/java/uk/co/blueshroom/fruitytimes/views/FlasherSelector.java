package uk.co.blueshroom.fruitytimes.views;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import uk.co.blueshroom.fruitytimes.objects.TextIcon;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

public class FlasherSelector extends View implements AnimatorListener, AnimatorUpdateListener {

	final String TAG = "FlasherSelector";
	public static enum BonusSelection { LOSE, HI_LOW, XTRA_LIFE, GAME_OVER, RANDOM_ICON, EXTRA_CASH, BONUS_GAME, EXTRA_HITS }
	
	ArrayList<TextIcon> displayed_values;
	int value_index = 0;
	
	ObjectAnimator randomFlashAnim;
	boolean isFlashing = false;
	
	ArrayList<String> kidStrings;
	ArrayList<BonusSelection> kidBonuses;
	
	public FlasherSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
		setWillNotDraw(false);
		displayed_values = new ArrayList<TextIcon>();
		kidStrings = new ArrayList<String>();
		kidBonuses = new ArrayList<BonusSelection>();
	}

	
	public void addChildren(List<String> children, List<BonusSelection> bonuses) {	
		
		final int PADDING = 8;
		int offsetRemain = getHeight() - ((children.size() + 1) * PADDING);
		int iconHeight = (int)((float)offsetRemain / (float)children.size());
		
		displayed_values = new ArrayList<TextIcon>();
		for(int i = 0; i < children.size(); i++) {
			displayed_values.add(i, new TextIcon(children.get(i), bonuses.get(i)));
			displayed_values.get(i).addAnimatorListener(this);
			displayed_values.get(i).addAnimatorUpdateListener(this);
			displayed_values.get(i).setDestRect(PADDING, PADDING + (i * iconHeight) + (i * PADDING), getWidth() - PADDING, PADDING + (i * iconHeight) + (i * PADDING) + iconHeight);			
		}
		
		value_index = new Random().nextInt(children.size());
		
		invalidate();
	}
	
	
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);		
		
		if(!displayed_values.isEmpty()) {			
			kidStrings.clear();
			kidBonuses.clear();
			
			for(TextIcon icon : displayed_values) { 
				kidStrings.add(icon.TEXT);
				kidBonuses.add(icon.BONUS);
			}
			
			addChildren(kidStrings, kidBonuses);				
		}
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		if(!displayed_values.isEmpty()) {
			for(TextIcon icon : displayed_values) 
				icon.draw(canvas);
		}
	}
	
	
	public void startRandomFlashing() {
		isFlashing = true;
		displayed_values.get(value_index).doFlash();
	}
	
	public void stopRandomFlashing() {
		isFlashing = false;
		displayed_values.get(value_index % displayed_values.size()).doFlash(); 
	}
	
	
	public void startIdleFlashing() {
		displayed_values.get(value_index).doIdleFlash();
	}
	
	public void stopIdleFlashing() {
		
	}
	
	public boolean getIsFlashing() {
		return isFlashing;
	}
	public TextIcon getSelectedTextIcon() {
		if(!displayed_values.isEmpty()) {
			stopRandomFlashing();
			startIdleFlashing();
			return displayed_values.get(value_index % displayed_values.size());			
		}
			
		
		return null;
	}

	
	
	
	

	@Override
	public void onAnimationCancel(Animator animator) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onAnimationEnd(Animator animator) {
		if(isFlashing) {
			int oldValue = Integer.valueOf(value_index);
			do value_index = new Random().nextInt(displayed_values.size());
			while(oldValue == value_index);
			
			displayed_values.get(value_index).doFlash();
		}	
		
		
	}


	@Override
	public void onAnimationRepeat(Animator animator) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onAnimationStart(Animator animator) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onAnimationUpdate(ValueAnimator animation) {
		invalidate();
		
	}
}
