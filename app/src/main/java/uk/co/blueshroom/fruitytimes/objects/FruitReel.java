package uk.co.blueshroom.fruitytimes.objects;

import java.util.Random;

import uk.co.blueshroom.fruitytimes.views.Reel;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;

public class FruitReel extends Reel {
	
	final String TAG = "FruitReel";	
	
	public FruitReel(Context context, AttributeSet attrs) {
		super(context, attrs);		
		
		for(int i = 0 ; i < MIDDLE_IC_COUNT; i++) {
			middleReelIcons[i] = new FruitReelIcon(new Random().nextInt(8));
			
			if(i < displayIcons.length) {
				displayIcons[i] = new FruitReelIcon(new Random().nextInt(8));		
				displayIcons[i].addAnimatorUpdateListener(this);
			}
			
			if(i < topReelIcons.length)
				topReelIcons[i] = new FruitReelIcon(new Random().nextInt(8));
			
			if(i < bottomReelIcons.length)
				bottomReelIcons[i] = new FruitReelIcon(new Random().nextInt(8));
		}	
		
		
		redrawSpinBitmap();
		initializeAnimators();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		int height = MeasureSpec.getSize(heightMeasureSpec);
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int length = (width < height / getDisplayedIconCount()) ?  width : height / getDisplayedIconCount();

		if(spinBitmap != null) {
			final int IC_COUNT = topReelIcons.length + middleReelIcons.length + bottomReelIcons.length;
			drawSrcRect.set(0, spinBitmap.getHeight() - ((spinBitmap.getHeight() / IC_COUNT) * displayIcons.length), spinBitmap.getWidth(), spinBitmap.getHeight());


			drawDestRect.set((width - length) / 2
					, (height - (length * getDisplayedIconCount())) / 2
					, width - ((width - length) / 2)
					, height - ((height - (length * getDisplayedIconCount())) / 2));


			setIconDestRects();
		}
	}
	
	@Override
	public Bitmap getReelCoverBmp() {
		return FruitsTheme.getReelCover();
	}
	@Override
	public Rect getReelCoverSrcRect() {
		return FruitsTheme.getReelCoverSrcRect();
	}
	@Override
	public int getDisplayedIconCount() {
		return 3;
	}
	
	
	@Override
	public void onAnimationEnd(Animator animation) {
		isSpinning = false;
		
	}

	@Override
	public void onAnimationRepeat(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animator animation) {		
		isSpinning = true;
		
	}

	@Override
	public void onAnimationCancel(Animator animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation) {
		invalidate();
		
	}	
}
