package uk.co.blueshroom.fruitytimes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.blueshroom.fruitytimes.R;

import uk.co.blueshroom.fruitytimes.data.Spins_DB;
import uk.co.blueshroom.fruitytimes.fragments.FragmentBonus;
import uk.co.blueshroom.fruitytimes.fragments.FragmentFeatureIconIndicator;
import uk.co.blueshroom.fruitytimes.fragments.FragmentFruitIconIndicator;
import uk.co.blueshroom.fruitytimes.fragments.FragmentFruitMachineButtons;
import uk.co.blueshroom.fruitytimes.fragments.FragmentFruitReels;
import uk.co.blueshroom.fruitytimes.fragments.FragmentLCDScreen;
import uk.co.blueshroom.fruitytimes.objects.BoardsTheme;
import uk.co.blueshroom.fruitytimes.objects.BonusSpinner;
import uk.co.blueshroom.fruitytimes.objects.FruitsController;
import uk.co.blueshroom.fruitytimes.objects.FruitsTheme;

public class ActivityFruitMachine extends Activity implements FragmentFruitMachineButtons.OnFragmentInteractionListener, FragmentFruitReels.OnSpinListener, BonusSpinner.BonusListener {

    FruitsController fruitsController;
    FragmentFruitMachineButtons fMachineButtons;
    FragmentFruitReels fruitReels;
    FragmentFruitIconIndicator fruitIconIndicator;
    FragmentFeatureIconIndicator featureIconIndicator;
    FragmentLCDScreen lcdScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit_machine);

        fruitsController = new FruitsController();

        fMachineButtons = (FragmentFruitMachineButtons)getFragmentManager().findFragmentById(R.id.fmachine_fragment_play_buttons);
        fruitReels = (FragmentFruitReels)getFragmentManager().findFragmentById(R.id.fmachine_fragment_reels);
        fruitIconIndicator = (FragmentFruitIconIndicator)getFragmentManager().findFragmentById(R.id.fmachine_fragment_fruiticons);
        featureIconIndicator = (FragmentFeatureIconIndicator)getFragmentManager().findFragmentById(R.id.fmachine_fragment_featureicons);
        lcdScreen = (FragmentLCDScreen)getFragmentManager().findFragmentById(R.id.fmachine_fragment_lcd);

        fruitIconIndicator.setReelBitmap(FruitsTheme.getReelBitmap(), 10);
        featureIconIndicator.setReelBitmap(FruitsTheme.getFeatureIcon(), 1);
    }

    @Override
    public void onStart() {
        super.onStart();
        fMachineButtons.setStake((int)fruitsController.getStake());
    }

    @Override
    public void onFragmentViewInteraction(int viewID) {
        int pos = 0;
        switch(viewID) {
            case R.id.fmachine_btn_hold3: pos++;
            case R.id.fmachine_btn_hold2: pos++;
            case R.id.fmachine_btn_hold1: fruitReels.holdReel(pos);

                break;

            case R.id.fmachine_btn_stake:
                fruitsController.changeStake();
                fMachineButtons.setStake((int)fruitsController.getStake());
                break;
            case R.id.fmachine_btn_spin:
                if(!fruitReels.isSpinning()) {
                    featureIconIndicator.allIndicatorsOff();
                    fruitIconIndicator.allIndicatorsOff();
                    fruitReels.spinReels(FruitsController.getWinType());
                }
                break;

        }
    }

    @Override
    public void onSpinFinished(float[][] spin_result) {
        fruitIconIndicator.addToIndicators(spin_result);
        featureIconIndicator.addToIndicators(spin_result);

        if(featureIconIndicator.isCollectionComplete()) {
            BoardsTheme.setTHEME(getResources(), BoardsTheme.THEME_ONE);
            startActivity(new Intent(getApplicationContext(), ActivityBoard.class));
        } else if(fruitIconIndicator.isCollectionComplete())
            FragmentBonus.getInstance().show(getFragmentManager(), FragmentBonus.TAG);

        fMachineButtons.setHoldReelEnabled(fruitsController.shouldEnableHold());
    }

    @Override
    public void onWin(FragmentFruitReels.WinObject win) {
        lcdScreen.addToBank(fruitsController.getWinAmount(win.winIndex));
    }

    @Override
    public void onBonusSpinComplete(BonusSpinner.Bonus bonus) {
        switch(bonus.bonusType) {
            case LOSE:
            case NO_BONUS:
                break;
            case FEATURE:
                fruitReels.spinReels(Spins_DB.WinType.FEATURE);
                break;
            case CASH:
                lcdScreen.addToBank(bonus.prizeAmount);
                break;
            case WINSPIN:
                fruitReels.spinReels(FruitsController.getWinSpinType());
                break;
        }

        featureIconIndicator.allIndicatorsOff();
        fruitIconIndicator.holdCollection(false);
        fruitIconIndicator.allIndicatorsOff();

    }
}
